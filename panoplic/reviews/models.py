import os

from django.conf import settings
from django.core.mail import send_mail
from django.db import models
from django.db.models import F
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.deconstruct import deconstructible
from django.utils.translation import ugettext_lazy as _

from panoplic.storage import OverwriteStorage


@deconstructible
class ReviewImgPath(object):
    def __init__(self, suffix):
        self.suffix = suffix

    def __call__(self, instance, filename):
        return os.path.join('reviews', instance.company.country.code,
                            str(instance).replace(' ', '_') + self.suffix)


rev_img1_path = ReviewImgPath('_image1')

rev_img2_path = ReviewImgPath('_image2')

rev_img3_path = ReviewImgPath('_image3')


class Review(models.Model):

    STAR_CHOICES = [(n, n,) for n in range(1, 6)]

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    company = models.ForeignKey('companies.Company')
    rating = models.PositiveSmallIntegerField()
    net_rating = models.FloatField(default=0.0)
    title = models.CharField(max_length=50)
    text = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    upvotes = models.IntegerField(default=0)
    image1 = models.ImageField(upload_to=rev_img1_path, blank=True,
                               storage=OverwriteStorage())
    image2 = models.ImageField(upload_to=rev_img2_path, blank=True,
                               storage=OverwriteStorage())
    image3 = models.ImageField(upload_to=rev_img3_path, blank=True,
                               storage=OverwriteStorage())

    class Meta:
        verbose_name = _('Review')
        verbose_name_plural = _('Reviews')
        unique_together = ('user', 'company')

    def set_net_rating(self):
        HINDERANCE_DEGREE = 1/10  # Degree of the root that curbs upvote impact
        DAILY_REDUCTION = 0.0004  # Age coefficient drops by this much each day
        NO_VOTES_VALUE = 0.1  # if self.upvotes == 0, this will be used instead
        MIN_AGE_FACTOR = 0.1

        age = timezone.now() - self.created_on
        age_factor = max((1 - age.days * DAILY_REDUCTION), MIN_AGE_FACTOR)
        self.net_rating = F('rating') * age_factor \
            * (self.upvotes or NO_VOTES_VALUE) ** (HINDERANCE_DEGREE)
        self.save(update_fields=['net_rating'])

    def set_upvotes(self):
        self.upvotes = Upvote.objects.filter(review=self).count()
        self.save(update_fields=['upvotes'])

    def send_notification(self):
        """
        Send an email to the relevant company notifying that it has just
        received a new review.
        """
        try:
            recipient = self.company.userprofile_set.first().user.email
        except AttributeError:
            return

        context_dict = {
            'reviewer': self.user
        }
        message = render_to_string(
            'email_templates/new_review.txt',
            context_dict
        )
        html_message = render_to_string(
            'email_templates/new_review.html',
            context_dict
        )
        send_mail(
            subject=_("Su empresa ha recibido una nueva crítica"),
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[recipient],
            message=message,
            html_message=html_message,
            fail_silently=True
        )

    def __str__(self):
        return "Review of {} by {}".format(self.company, self.user)


class Upvote(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    review = models.ForeignKey(Review)

    class Meta:
        unique_together = ('user', 'review')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.review.set_upvotes()

    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        self.review.set_upvotes()

    def __str__(self):
        return "{}'s vote for: {}".format(self.user, self.review)
