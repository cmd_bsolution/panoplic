from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^(?P<pk>\d+)/edit/$',
        views.ReviewUpdateView.as_view(),
        name='edit_review'
    ),
    url(
        r'^(?P<pk>\d+)/delete/$',
        views.ReviewDeleteView.as_view(),
        name='delete_review'
    ),
    url(
        r'^upvote/(?P<review_id>\d+)/$',
        views.upvote,
        name='upvote'
    ),
]
