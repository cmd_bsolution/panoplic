from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import get_object_or_404, render_to_response
from django.utils.translation import ugettext as _
from django.views.generic.edit import DeleteView, UpdateView

from utils.views import TestOwnerMixin
from .models import Review, Upvote
from .forms import ReviewForm


# Text for the reviews' upvote/downvote buttons.
UPVOTE_TEXT = _("Me fue útil")
DOWNVOTE_TEXT = _("Ya no me es útil")


class ReviewUpdateView(TestOwnerMixin, UpdateView):

    model = Review
    form_class = ReviewForm
    template_name = 'review_edit_form.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)

        response_data = {
            'valid': True,
            'rating': self.object.rating,
            'text': self.object.text,
            'deleted_images': [],
        }

        images_to_delete = self.request.POST.get('deletethis')
        if images_to_delete:
            for image in images_to_delete.split():
                self.object.__dict__[image].delete(save=False)
                response_data['deleted_images'].append(image)

        self.object.save()
        response_data.update(mean_rating=self.object.company.set_mean_rating())
        timestamp = "?" + datetime.now().strftime("%m-%d_%H:%M:%S")
        if self.object.image1:
            response_data.update(image1_url=self.object.image1.url + timestamp)
        if self.object.image2:
            response_data.update(image2_url=self.object.image2.url + timestamp)
        if self.object.image3:
            response_data.update(image3_url=self.object.image3.url + timestamp)

        return JsonResponse(response_data)

    def form_invalid(self, form):
        return render_to_response(self.get_template_names(),
                                  self.get_context_data(form=form))


class ReviewDeleteView(TestOwnerMixin, DeleteView):
    model = Review

    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched Review object, update the
        company's mean rating and generate the response.
        """
        self.object = self.get_object()
        self.object.delete()
        self.object.company.set_mean_rating()
        return render_to_response('partial/new_review_prompt.html')


@login_required
def upvote(request, review_id):
    if request.method == 'POST':
        review = get_object_or_404(Review, pk=review_id)
        if request.POST['button'] == 'upvoted':
            upvote = Upvote(review=review, user=request.user)
            upvote.save()
            response_data = {'upvoted': True,
                             'new_btn_value': DOWNVOTE_TEXT}
        else:
            Upvote.objects.get(user=request.user, review=review).delete()
            response_data = {'upvoted': False,
                             'new_btn_value': UPVOTE_TEXT}
        review.refresh_from_db()
        response_data['upvotes'] = review.upvotes
        return JsonResponse(response_data)
