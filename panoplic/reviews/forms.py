from django import forms
from django.utils.translation import ugettext_lazy as _

from imagekit.forms import ProcessedImageField
from imagekit.processors import ResizeToFill

from utils.forms import validate_size
from .models import Review, Upvote


class ReviewForm(forms.ModelForm):
    rating = forms.TypedChoiceField(widget=forms.RadioSelect,
                                    choices=Review.STAR_CHOICES,
                                    required=True)
    image1 = ProcessedImageField(
        widget=forms.FileInput,
        spec_id='reviews:review:image1',
        processors=[ResizeToFill(320, 180, upscale=False)],
        format='JPEG',
        validators=[validate_size],
        required=False
    )
    image2 = ProcessedImageField(
        widget=forms.FileInput,
        spec_id='reviews:review:image2',
        processors=[ResizeToFill(320, 180, upscale=False)],
        format='JPEG',
        validators=[validate_size],
        required=False
    )
    image3 = ProcessedImageField(
        widget=forms.FileInput,
        spec_id='reviews:review:image3',
        processors=[ResizeToFill(320, 180, upscale=False)],
        format='JPEG',
        validators=[validate_size],
        required=False
    )

    class Meta:
        model = Review
        fields = ('rating', 'title', 'text', 'image1', 'image2', 'image3')

    def __init__(self, company=None, user=None, *args, **kwargs):
        super(ReviewForm, self).__init__(*args, **kwargs)
        self.company = company
        self.user = user

    def clean(self):
        if self.company and self.user and \
                Review.objects.filter(company=self.company, user=self.user).exists():
            raise forms.ValidationError(_('You have already reviewed this company.'))
        return self.cleaned_data

    def save(self, commit=True):
        instance = super(ReviewForm, self).save(commit=False)
        if not hasattr(instance, 'user') and self.user is not None:
            instance.user = self.user
        if not hasattr(instance, 'company') and self.company is not None:
            instance.company = self.company

        if commit:
            instance.save()
        return instance


class UpvoteForm(forms.ModelForm):

    class Meta:
        model = Upvote
        fields = ()
