from django.test import TestCase
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify

from users.models import UserProfile
from companies.models import Company, Country

from ..models import Review

HOST = 'test.panoplic-dev.com'
UserModel = get_user_model()


def create_company(name, active=False):
    country = Country.objects\
                     .get_or_create(name='Test Country', code='xx')[0]
    return Company.objects.create(name=name, active=active, country=country)


class ReviewUpdateTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.company = create_company("Test Company", active=True)
        user = UserModel.objects.create_user('testuser', 'a@b.com', 'testpw')
        UserProfile.objects.create(user=user)

        cls.review = Review.objects.create(
            rating=5,
            title='Review',
            text='Review body',
            user=user,
            company=cls.company
        )

    def setUp(self):
        self.client.login(username='testuser', password='testpw')

    def test_update(self):
        data = {
            'rating': 5,
            'title': 'I really liked it',
            'text': 'It\'s a good company',
        }
        response = self.client.post(
            reverse('edit_review', args=(self.review.id,)),
            data=data,
            HTTP_HOST=HOST
        )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.json()['valid'])
