from django.conf import settings
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.contrib.sitemaps import views as sitemaps_views
from django.views.static import serve

from users.forms import CustomAuthenticationForm
from users.views import CustomRegistrationView, PasswordChangeView
from companies.sitemap import CompanySitemap
from .admin import admin

sitemaps = {'company': CompanySitemap}

urlpatterns = [
    url(
        r'^accounts/register/$',
        CustomRegistrationView.as_view(),
        name='registration_register'
    ),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(
        r'^login/$',
        auth_views.login,
        {'authentication_form': CustomAuthenticationForm},
        name='auth_login'
    ),
    url(
        r'^logout/$',
        auth_views.logout,
        {'next_page': '/'},
        name='auth_logout'
    ),
    url(
        r'^password-change/$',
        PasswordChangeView.as_view(),
        name='password_change'
    ),
    url(r'^o6t5uzk7/', include(admin.site.urls)),
    url(r'', include('companies.urls')),
    url(r'^users/', include('users.urls')),
    url(r'^search/', include('search.urls')),
    url(r'^reviews/', include('reviews.urls')),
    url(r'^reports/', include('reports.urls')),
    url('', include('social_django.urls', namespace='social')),
    url(
        r'^sitemap\.xml$',
        sitemaps_views.index,
        {'sitemaps': sitemaps},
        name='sitemap_index'
    ),
    url(
        r'^sitemap-(?P<section>.+)\.xml$',
        sitemaps_views.sitemap,
        {'sitemaps': sitemaps},
        name='sitemap'
    ),
    url(r'^api/', include('panoplic.routes')),
]

if settings.DEBUG:
    urlpatterns += [
        url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], serve,
            {"document_root": settings.MEDIA_ROOT})
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns = [url(r'^__debug__/', include(debug_toolbar.urls))] + urlpatterns
