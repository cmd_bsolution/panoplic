from django.conf import settings
from django.conf.urls import url, include
from django.views.generic import TemplateView, RedirectView

urlpatterns = [
    url(
        r'^$',
        TemplateView.as_view(template_name='countries.html'),
        name='countries'
    ),
    url(
        r'^.+$',
        RedirectView.as_view(pattern_name='countries', permanent=True)
    )
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
