class SubdomainMiddleware(object):
    """
    Store subdomain name as an attribute of the request object, selecting
    an alternative urlconf in case there is no subdomain.
    """
    host_length = 2

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        domain_parts = request.get_host().replace('www.', '').split('.')
        if len(domain_parts) > self.host_length:
            subdomain = domain_parts[0]
        else:
            subdomain = None
            request.urlconf = 'panoplic.urls_int'
        request.subdomain = subdomain

        response = self.get_response(request)
        return response


class TempDomainSubdomainMiddleware(SubdomainMiddleware):
    """SubdomainMiddleware for the 3-part temporary domain."""
    host_length = 3


class ArSubdomainMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.subdomain = 'ar'

        response = self.get_response(request)
        return response
