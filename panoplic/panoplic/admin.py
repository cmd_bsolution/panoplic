from django.contrib import admin

admin.site.site_header = 'Panoplic administration'
admin.site.site_title = 'Panoplic administration'
admin.site.index_title = 'Index'
