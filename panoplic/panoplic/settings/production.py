from .base import *  # noqa

ALLOWED_HOSTS = ['.panoplic.com']

MIDDLEWARE += ('panoplic.middleware.SubdomainMiddleware',)

ADMINS = (
    ('dev', 'dev@panoplic.com'),
)

MANAGERS = (
    ('info', 'info@panoplic.com'),
)

RAVEN_CONFIG = {
    'dsn': 'https://2d253e3871aa4e06a889113886bafee5:'
           '9210fb264c9445349fa363661b7b6bae@app.getsentry.com/54051'
}
