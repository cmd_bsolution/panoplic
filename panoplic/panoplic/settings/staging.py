from .base import *  # noqa

ALLOWED_HOSTS = ['.crandi1.ferozo.com']

MIDDLEWARE += ('panoplic.middleware.TempDomainSubdomainMiddleware',)

ADMINS = (
    ('dev', 'dev@panoplic.com'),
)

MANAGERS = (
    ('info', 'info@panoplic.com'),
)

RAVEN_CONFIG = {
    'dsn': 'https://ab4a6d2d18254aadaf00f7c35eac4439:'
           '20f7538901c6436fbeaaea1e092635c6@app.getsentry.com/54052'
}
