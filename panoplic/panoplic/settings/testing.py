from .base import *  # noqa

ALLOWED_HOSTS = ['.crandi1.ferozo.com']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['POSTGRES_DB'],
        'USER': os.environ['POSTGRES_USER'],
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': os.environ.get('POSTGRES_HOST', '127.0.0.1'),
        'PORT': '',
    }
}

MIDDLEWARE += ('panoplic.middleware.TempDomainSubdomainMiddleware',)
