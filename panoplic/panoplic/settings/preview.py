from .base import *  # noqa

ALLOWED_HOSTS = ['*']

MIDDLEWARE += ('panoplic.middleware.ArSubdomainMiddleware',)

ADMINS = (
    ('dev', 'dev@panoplic.com'),
)

MANAGERS = (
    ('info', 'info@panoplic.com'),
)
