import sys
import socket
import os

from .base import *  # NOQA

DEBUG = True

ALLOWED_HOSTS = ['.panoplic-dev.com']

EMAIL_HOST = os.environ.get('EMAIL_HOST', 'localhost')
EMAIL_PORT = 1025


INSTALLED_APPS += ("debug_toolbar",)

INTERNAL_IPS = ['127.0.0.1', '10.0.2.2', ]

# tricks to have debug toolbar when developing with docker
if os.environ.get('USE_DOCKER') == 'yes':
    ip = socket.gethostbyname(socket.gethostname())
    INTERNAL_IPS += [ip[:-1] + '1']

DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': [
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ],
    'SHOW_TEMPLATE_CONTEXT': True,
}

# django_coverage_plugin needs this to be explicitly enabled.
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

MIDDLEWARE += (
    "panoplic.middleware.SubdomainMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
)

MANAGERS = (
    ('Dev Testing', 'dev@panoplic.com'),
)

if 'test' in sys.argv:
    DATABASES['default'] = {'ENGINE': 'django.db.backends.sqlite3'}


# class DisableMigrations(object):
#
#     def __contains__(self, item):
#         return True
#
#     def __getitem__(self, item):
#         return "notmigrations"
#
# MIGRATION_MODULES = DisableMigrations()


REST_FRAMEWORK = {
    'DEFAULT_VERSION': LATEST
}
