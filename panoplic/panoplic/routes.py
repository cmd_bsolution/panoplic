from rest_framework.routers import DefaultRouter
from companies.viewsets import CompanyViewSet, ProductViewSet, ServiceViewSet


router = DefaultRouter()
router.register(r'companies', CompanyViewSet)
router.register(r'products', ProductViewSet)
router.register(r'services', ServiceViewSet)

urlpatterns = router.urls
