from django.test import LiveServerTestCase

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


BRAND_NAME = 'Panoplic'


class VisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Chrome()
        self.browser.implicitly_wait(5)

    def tearDown(self):
        self.browser.quit()

    def test_search(self):
        # Browse to homepage.
        self.browser.get(self.live_server_url)

        # Check for brand name in title.
        self.assertIn(BRAND_NAME, self.browser.title)

        # The search box should be there.
        try:
            search_box = self.browser.find_element_by_id('search_box')
        except NoSuchElementException:
            self.fail("Unable to find the #search_box")

        # Try a search. It should go to the search results page.
        search_box.send_keys('alarmas')
        search_box.submit()

        self.assertIn('search', self.browser.current_url)
