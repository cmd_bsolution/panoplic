import csv
import sys

from django.contrib.auth.models import User


def delete_users(csvfile):
    """
    Delete users whose company name is listed in the CSV file.
    """
    with open(csvfile, newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            try:
                user = User.objects.get(profile__company__name=row[0])
                user.delete()
            except User.DoesNotExist:
                pass


def run(*args):
    """
    function intended for use with django-extnsions's runscript.
    arguments passed with --script-args <argument>.
    """
    if len(args) == 1:
        delete_users(args[0])
    else:
        print("Enter the name of the CSV file as argument.")
        sys.exit(0)
