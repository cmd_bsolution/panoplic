import os
import csv
import sys

from companies.models import Category, Product


def categorize(csvfile):
    """
    Put companies which offer certain products into the corresponding
    categories, according to the csvfile.
    """
    with open(csvfile, newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            for name in row[1:]:
                prod = Product.objects.get_or_create(name=name)[0]
                prod.category = Category.objects.get(name=row[0])
                prod.save()
                print("Placed product " + str(prod) + "in category" + prod.name)


def run(*args):
    """
    function intended for use with django-extnsions's runscript.
    arguments passed with --script-args <argument>.
    """
    if len(args) == 1:
        categorize(args[0])
    else:
        print("Usage: " + os.path.basename(__file__)
                        + " --script-args csvfile")
        sys.exit(0)
