import os
import csv
import sys

from django.db import IntegrityError

from companies.models import Company, Country, Province, City, Product


def create_comp(csvfile, country_name):
    """
    Creates a series of Company objects, with fields taken from csvfile.
    --------------------------------------------------------------------------

    Each line in the CSV file should contain data for one company, formatted
    like so:
        Name,Address,City Name,Phone Number,Website,Logo Filename,Product Name

    The only required field is the company's name.
    Duplicated entries will be skipped.
    --------------------------------------------------------------------------
    """

    # model max_length restrictions
    NAME_MAX_LENGTH = 50
    LEGAL_NAME_MAX_LENGTH = 50
    ADDRESS_MAX_LENGTH = 50
    PHONE_MAX_LENGTH = 50
    WEBSITE_MAX_LENGTH = 100
    PRODUCT_NAME_MAX_LENGTH = 50

    try:
        country = Country.objects.get(name=country_name)
    except Country.DoesNotExist:
        print("{} is not a valid country name, please try again."
              .format(country_name))
        sys.exit(1)

    created = 0  # Counter for successfully saved companies.
    skipped = 0  # Counter for duplicated companies.

    with open(csvfile, newline='') as f:
        reader = csv.reader(f)
        for row in reader:

            try:
                city = City.objects.get(name=row[2], province__country=country)
                province = city.province
            except City.MultipleObjectsReturned:
                city = None
                province = None
            except City.DoesNotExist:
                # If the city with this name doesn't exist, it may instead
                # be the name of a province.
                try:
                    city = None
                    province = Province.objects.get(name=row[2],
                                                    country=country)
                except Province.DoesNotExist:
                    city = None
                    province = None

            comp = Company(
                country=country,
                name=row[0][:NAME_MAX_LENGTH],
                legal_name=row[0][:LEGAL_NAME_MAX_LENGTH],
                address=row[1][:ADDRESS_MAX_LENGTH],
                city=city,
                province=province,
                phone=row[3][:PHONE_MAX_LENGTH],
                website=row[4][:WEBSITE_MAX_LENGTH],
                active=True
            )
            try:
                comp.save()
            except IntegrityError:
                print('Company ' + comp.name + ' already exists. Skipping...')
                skipped += 1
                continue
            else:
                print('Company ' + comp.name + ' created')
                created += 1

            print("Setting logos...")
            if row[5]:
                comp.logo = os.path.join('companies', country.code, row[5])
                comp.save()

            print("Setting products...")
            product_name = row[6].capitalize()[:PRODUCT_NAME_MAX_LENGTH]
            prod = Product.objects.get_or_create(name=product_name)[0]
            comp.products.add(prod)
            comp.save()

    print("{} companies created.".format(created))
    print("{} duplicates skipped.".format(skipped))


def run(*args):
    """
    function intended for use with django-extnsions's runscript.
    arguments passed with --script-args <argument>.
    """
    if len(args) == 2:
        create_comp(args[0], args[1])
    else:
        print("Usage: " + os.path.basename(__file__)
                        + " --script-args csv-file-path country-name")
        sys.exit(0)
