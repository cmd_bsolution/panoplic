import os
import csv
import sys

from companies.models import Country, Province, City


def create_provinces_cities(csvfile, country_name):
    """
    Creates Province and City objects from fields in a CSV file.
    Each line of the file must contain the name of a place (first level of
    geographical organization) in the first field, followed by any number of
    subdivisions of that place in subsequent ones.

    Like this:
        Province1,City11,City12,City13,City14,...,City1n
        Province2,City21,City22,City23,City24,...,City2m
        .
        .
        .
        ProvinceN,CityN1,CityN2,CityN3,CityN4,...,CityNM
    """

    try:
        country = Country.objects.get(name=country_name)
    except Country.DoesNotExist:
        print("{} is not a valid country name, please try again."
              .format(country_name))
        sys.exit(1)

    with open(csvfile, newline='') as f:
        reader = csv.reader(f)
        for row in reader:
            province = Province.objects.create(name=row[0], country=country)
            print("Creating {}".format(province))
            for city_name in row[1:]:
                city = City.objects.create(name=city_name, province=province)
                print("Creating {}".format(city))


def run(*args):
    """
    function intended for use with django-extnsions's runscript.
    arguments passed with --script-args <argument>.
    """
    if len(args) == 2:
        create_provinces_cities(args[0], args[1])
    else:
        print("Usage: " + os.path.basename(__file__)
                        + " --script-args csvfile country-name")
        sys.exit(0)
