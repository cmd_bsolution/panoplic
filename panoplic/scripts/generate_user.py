import csv
import sys
import random
import string

from django import db
from django.contrib.auth.models import User

from companies.models import Company
from users.models import UserProfile


def create_users(csvin, csvout):
    """
    Create a series of user and their respective profiles, tying them to a
    Company.
    """
    atdomain = '@panoplic.com'
    # Maximum email lenght of 75 chars hardcoded in Django
    LOCALPART_LIMIT = 75 - len(atdomain)
    MAX_USERNAME_LEN = 30
    PW_LEN = 16

    with open(csvin, newline='') as infile,\
            open(csvout, 'w', newline='') as outfile:
        reader = csv.reader(infile)
        writer = csv.writer(outfile)
        charmap = {'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u', '?': '',
                   '-': '', ' ': '', ' - ': '_'}

        for row in reader:
            compname = row[0]
            username = compname[:MAX_USERNAME_LEN].lower()
            for old, new in charmap.items():
                username = username.replace(old, new)
            email = username[:LOCALPART_LIMIT] + atdomain
            pw = ''.join(random.choice(string.ascii_letters + string.digits)
                         for i in range(PW_LEN))

            try:
                comp = Company.objects.get(name=compname)
            except Company.DoesNotExist:
                print('Company ' + compname + ' does not exist. Skipping...')
                comp = None

            if comp:
                try:
                    user = User.objects.create_user(
                        username=username,
                        email=email,
                        password=pw,
                    )
                    user.save()
                    profile = UserProfile(
                        user=user,
                        province=comp.province,
                        country=comp.country,
                        company=comp
                    )
                    profile.save()
                    writer.writerow([compname, email, pw])
                except db.IntegrityError:
                    print('An error ocurred. Possible duplicate')
                    pass


def run(*args):
    """
    function intended for use with django-extnsions's runscript.
    arguments passed with --script-args <argument>.
    """
    if len(args) == 2:
        create_users(args[0], args[1])
    else:
        print("Enter the name of the input and output files as arguments.")
        sys.exit(0)
