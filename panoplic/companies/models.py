import os

from django.conf import settings
from django.core.validators import MinValueValidator
from django.contrib.sites.models import Site
from django.contrib.sites.requests import RequestSite
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Avg
from django.template.loader import render_to_string
from django.utils.deconstruct import deconstructible
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from taggit.managers import TaggableManager

from panoplic.storage import OverwriteStorage
from reviews.models import Review
from .manager import CompanyManager


class Country(models.Model):

    name = models.CharField(max_length=50, unique=True)
    code = models.CharField(max_length=2, unique=True)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __str__(self):
        return self.name


class Province(models.Model):

    name = models.CharField(max_length=50)
    country = models.ForeignKey(Country)

    class Meta:
        verbose_name = _('Province')
        verbose_name_plural = _('Provinces')

    def __str__(self):
        return self.name


class City(models.Model):

    name = models.CharField(max_length=50)
    province = models.ForeignKey(Province)

    class Meta:
        verbose_name = _('City')
        verbose_name_plural = _('Cities')

    def __str__(self):
        return self.name


class Category(models.Model):

    name = models.CharField(max_length=50, unique=True)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __str__(self):
        return self.name


class Product(models.Model):

    name = models.CharField(max_length=50, unique=True)
    code = models.SlugField(unique=True)
    category = models.ForeignKey(Category, null=True)

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    def __str__(self):
        return self.name


class Service(models.Model):

    name = models.CharField(max_length=50, unique=True)
    code = models.SlugField(unique=True)

    class Meta:
        verbose_name = _('Service')
        verbose_name_plural = _('Services')

    def __str__(self):
        return self.name


class Offering(models.Model):
    company = models.ForeignKey('company')
    initial_price = models.DecimalField(
        max_digits=14,
        decimal_places=4,
        validators=[MinValueValidator(0)]
    )
    monthly_price = models.DecimalField(
        max_digits=14,
        decimal_places=4,
        validators=[MinValueValidator(0)]
    )

    class Meta:
        abstract = True


class ProductOffering(Offering):
    product = models.ForeignKey('Product')

    class Meta:
        verbose_name = _('Product Offer')
        verbose_name_plural = _('Product Offers')


class ServiceOffering(Offering):
    service = models.ForeignKey('Service')

    class Meta:
        verbose_name = _('Service Offer')
        verbose_name_plural = _('Service Offers')


@deconstructible
class CompanyImgPath(object):
    def __init__(self, suffix):
        self.suffix = suffix

    def __call__(self, instance, filename):
        return os.path.join('companies', instance.country.code,
                            slugify(instance.name) + self.suffix)


logo_path = CompanyImgPath('_logo')

comp_img1_path = CompanyImgPath('_image1')

comp_img2_path = CompanyImgPath('_image2')

comp_img3_path = CompanyImgPath('_image3')


class Company(models.Model):

    objects = CompanyManager()

    name = models.CharField(
        max_length=50, unique=True,
        verbose_name=_('Company Name')
    )
    description = models.TextField(
        max_length=250,
        blank=True,
        verbose_name=_('Description')
    )
    legal_name = models.CharField(max_length=50, verbose_name=_('Legal Name'))
    website = models.URLField(max_length=100, verbose_name=_('Website'))
    email = models.EmailField(
        max_length=255,
        verbose_name=_('email'),
        null=True
    )
    cuit = models.CharField(max_length=50, blank=True, verbose_name=_('CUIT'))
    phone = models.CharField(max_length=50, verbose_name=_('Phone'))
    address = models.CharField(max_length=50, verbose_name=_('Address'))
    address_number = models.CharField(
        max_length=50,
        verbose_name=_('Address Number')
    )
    contact_name = models.CharField(
        max_length=50,
        verbose_name=_('Contact Name'),
        blank=True
    )
    city = models.ForeignKey(City, null=True)
    province = models.ForeignKey(Province, null=True)
    country = models.ForeignKey(Country, null=True)
    products = models.ManyToManyField(Product, through='ProductOffering', blank=True)
    services = models.ManyToManyField(Service, through='ServiceOffering', blank=True)
    active = models.BooleanField(default=False)
    mark = models.FloatField(default=0.0)
    mean_rating = models.FloatField(null=True, blank=True)

    logo = models.ImageField(upload_to=logo_path,
                             default='companies/default.gif',
                             storage=OverwriteStorage())
    image1 = models.ImageField(upload_to=comp_img1_path, blank=True, null=True,
                               storage=OverwriteStorage())
    image2 = models.ImageField(upload_to=comp_img2_path, blank=True, null=True,
                               storage=OverwriteStorage())
    image3 = models.ImageField(upload_to=comp_img3_path, blank=True, null=True,
                               storage=OverwriteStorage())
    tags = TaggableManager(blank=True)

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')
        ordering = ('-mark',)

    def send_creation_email(self, request):
        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(request)
        context_dict = {'company': self, 'site': site}
        message = render_to_string('email_templates/new_company.txt', context_dict)
        html_message = render_to_string('email_templates/new_company.html', context_dict)
        send_mail(
            'Nueva empresa %s' % self.name,
            message=message,
            html_message=html_message,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[email for name, email in settings.MANAGERS],
            fail_silently=True,
        )

    def set_mean_rating(self):
        self.mean_rating = Review.objects.filter(company=self)\
                                 .aggregate(Avg('rating'))['rating__avg']
        self.save(update_fields=['mean_rating'])
        return self.mean_rating

    def set_mark(self):
        """
        Set the current mark, returning 1 if the Company has a rating
        and 0 if it doesn't.
        """
        if not self.mean_rating:
            return 0

        PE = 10  # Weight of the prior estimate.

        global_avg = Company.objects.filter(country=self.country)\
                            .aggregate(Avg('mean_rating'))['mean_rating__avg']
        reviews = self.review_set.all()
        votes = reviews.count()
        company_avg = reviews.aggregate(Avg('net_rating'))['net_rating__avg']
        self.mark = (company_avg * votes + global_avg * PE) / (votes * PE)
        self.save(update_fields=['mark'])
        return 1

    def get_reviewers(self):
        """Return a generator of users that have reviewed the company."""
        return (review.user for review in self.review_set.all())

    def get_absolute_url(self):
        return reverse('company_view',
                       kwargs={'pk': self.pk, 'slug': slugify(self.name)})

    def __str__(self):
        return self.name
