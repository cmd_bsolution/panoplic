from django.conf.urls import url, include
from django.views.generic import TemplateView

from . import views

segments = [
    url(
        r'^homes/$',
        TemplateView.as_view(template_name='segments/homes.html'),
        name='homes'
    ),
    url(
        r'^businesses/$',
        TemplateView.as_view(template_name='segments/businesses.html'),
        name='businesses'
    ),
    url(
        r'^vehicles/$',
        TemplateView.as_view(template_name='segments/vehicles.html'),
        name='vehicles'
    ),
    url(
        r'^enterprises/$',
        TemplateView.as_view(template_name='segments/enterprises.html'),
        name='enterprises'
    ),
]

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='home'),
    url(r'^segments/', include(segments, namespace='segments')),
    url(
        r'^terms-conditions/$',
        TemplateView.as_view(template_name='terms.html'),
        name='terms-conditions'
    ),
    url(
        r'^how-to-publish/$',
        TemplateView.as_view(template_name='howto_publish.html'),
        name='how-to-publish'
    ),
    url(
        r'^how-to-register/$',
        TemplateView.as_view(template_name='howto_register.html'),
        name='how-to-register'
    ),
    url(
        r'^how-it-works/$',
        TemplateView.as_view(template_name='how_works.html'),
        name='how-it-works'
    ),
    url(
        r'^scope/$',
        TemplateView.as_view(template_name='scope.html'),
        name='scope'
    ),
    url(
        r'^contact/$',
        TemplateView.as_view(template_name='contact.html'),
        name='contact'
    ),
    url(r'^404/$', TemplateView.as_view(template_name='404.html')),
    url(r'^403/$', TemplateView.as_view(template_name='403.html')),
    url(r'^500/$', TemplateView.as_view(template_name='500.html')),
    url(
        r'^companies/$',
        views.CompanyListView.as_view(),
        name='companies'
    ),
    url(
        r'^company/register$',
        views.company_register,
        name='company_register'
    ),
    url(
        r'^company/success$',
        TemplateView.as_view(template_name='company_success.html'),
        name='company_success'
    ),
    url(
        r'^company/(?P<pk>\d+)/(?P<slug>[\w-]+)/$',
        views.CompanyView.as_view(),
        name='company_view'
    ),
    url(
        r'^company/profile$',
        views.company_profile,
        name='company_profile'
    ),
    url(
        r'^upload_logo/$',
        views.upload_logo,
        name='upload_logo'
    ),
    url(
        r'^delete_logo/$',
        views.delete_logo,
        name='delete_logo'
    ),
    url(r'^populate-select/$', views.populate_select),
]
