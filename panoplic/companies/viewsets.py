from django.db.models import Prefetch

from rest_framework import viewsets

from .models import Company, Product, Service, ProductOffering, ServiceOffering
from .serializers import CompanySerializer, ProductSerializer, ServiceSerializer


class CompanyViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.prefetch_related(Prefetch(
        'productoffering_set',
        queryset=ProductOffering.objects.select_related('company')
    )).all()
    serializer_class = ProductSerializer
    lookup_field = 'code'


class ServiceViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Service.objects.prefetch_related(Prefetch(
        'serviceoffering_set',
        queryset=ServiceOffering.objects.select_related('company')
    )).all()
    serializer_class = ServiceSerializer
    lookup_field = 'code'
