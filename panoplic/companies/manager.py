from django.db import models


class CompanyManager(models.Manager):

    def get_queryset(self):
        return super(CompanyManager, self).get_queryset().filter(active=True)

    def all_include_inactive(self):
        return super(CompanyManager, self).get_queryset()
