from django.test import TestCase

from ..models import Company

from .factories import CompanyFactory


class TestCompanyManager(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.company1 = CompanyFactory.create(active=True)
        cls.company2 = CompanyFactory.create(active=True)
        cls.company3 = CompanyFactory.create(active=False)

    def test_query_active(self):
        self.assertEqual(Company.objects.count(), 2)
        self.assertQuerysetEqual(Company.objects.all(),
                                 [repr(c) for c in [self.company1, self.company2]])

    def test_query_everything(self):
        self.assertEqual(Company.objects.all_include_inactive().count(), 3)
        self.assertQuerysetEqual(Company.objects.all_include_inactive().all(),
                                 [repr(c) for c in [self.company1, self.company2, self.company3]])
