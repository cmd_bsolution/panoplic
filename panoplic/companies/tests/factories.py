import string

import factory
from factory import fuzzy

from ..models import Country, Province, City, Category, Product, Service, Company


class CountryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Country

    name = factory.Sequence("Country{}".format)
    code = factory.Sequence("c{}".format)


class ProvinceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Province

    name = factory.Sequence('Province{}'.format)
    country = factory.SubFactory(CountryFactory)


class CityFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = City

    name = factory.Sequence('City{}'.format)
    province = factory.SubFactory(ProvinceFactory)


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category

    name = factory.Sequence('Category{}'.format)


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product

    name = factory.Sequence('Product{}'.format)
    code = fuzzy.FuzzyText(chars=string.ascii_lowercase)


class ServiceFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Service

    name = factory.Sequence('Service{}'.format)
    code = fuzzy.FuzzyText(chars=string.ascii_lowercase)


class CompanyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Company

    name = factory.Sequence("Company{}".format)
    description = 'description!'
    legal_name = factory.Sequence("name{} inc.".format)
    website = 'example.com'
    email = 'email@example.com'
    cuit = 'str'
    phone = '123'
    address = 'address!'
    address_number = '123'
    contact_name = 'Mr, contact'
    country = factory.SubFactory(CountryFactory)
    province = factory.LazyAttribute(lambda o: ProvinceFactory.create(country=o.country))
    city = factory.LazyAttribute(lambda o: CityFactory.create(province=o.province))
    active = False
    logo = factory.django.ImageField(color='blue')
    image1 = factory.django.ImageField(color='green')
    image2 = factory.django.ImageField(color='white')
    image3 = factory.django.ImageField(color='black')
    tags = 'tag1,tag2,tag3'
