from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify
from django.test import TestCase

from reviews.forms import ReviewForm
from reviews.models import Review
from users.models import UserProfile
from ..forms import CompanyForm
from ..models import Company, Country, Province, City

from .factories import CompanyFactory


HOST = 'test.panoplic-dev.com'
user_model = get_user_model()


class IndexViewTest(TestCase):

    def test_renders_main_template(self):
        response = self.client.get(reverse('home'), HTTP_HOST=HOST)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_renders_search_template(self):
        response = self.client.get(reverse('home'), HTTP_HOST=HOST)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'partial/search.html')


class CompanyListViewTest(TestCase):

    def test_renders_main_template(self):
        response = self.client.get(reverse('companies'), HTTP_HOST=HOST)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'companies.html')


class CompanyDetailsTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.company = CompanyFactory.create(active=True)

    def setUp(self):
        self.response = self.client.get(
            reverse(
                'company_view',
                args=[self.company.id, slugify(self.company.name)]
            ),
            HTTP_HOST=HOST
        )

    def test_renders_main_template(self):
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'company_view.html')

    def test_uses_review_form(self):
        self.assertIsInstance(self.response.context['review_form'], ReviewForm)

    def test_wrong_slug(self):
        response = self.client.get(
            reverse(
                'company_view',
                args=[self.company.id, 'wrong-slug']
            ),
            HTTP_HOST=HOST
        )
        self.assertRedirects(
            response,
            reverse(
                'company_view',
                args=[self.company.id, slugify(self.company.name)]
            ),
            status_code=301,
            host=HOST,
            fetch_redirect_response=False
        )


class ReviewCreateTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.company = CompanyFactory.create(active=True)
        user = user_model.objects.create_user('testuser', 'a@b.com', 'testpw')
        UserProfile.objects.create(user=user)

    def setUp(self):
        self.client.login(username='testuser', password='testpw')

    def submit_review(self, data=None):
        if data is None:
            data = {
                'rating': 5,
                'title': 'I really liked it',
                'text': 'It\'s a good company',
            }
        return self.client.post(
            reverse(
                'company_view',
                args=[self.company.id, slugify(self.company.name)]
            ),
            data=data,
            HTTP_HOST=HOST
        )

    def test_create(self):
        self.submit_review()
        self.assertEqual(Review.objects.count(), 1)

    def test_create_redirect(self):
        response = self.submit_review()
        self.assertRedirects(
            response,
            reverse(
                'company_view',
                args=[self.company.id, slugify(self.company.name)]
            ) + '#arrange',
            status_code=302,
            host=HOST,
            fetch_redirect_response=False
        )

    def test_duplicate_review(self):
        self.submit_review()
        response = self.submit_review()
        self.assertEqual(response.status_code, 200)
        self.assertIn('__all__', response.context['form'].errors)

    def test_unvalid_create(self):
        response = self.submit_review({})
        self.assertEqual(Review.objects.count(), 0)
        self.assertEqual(response.status_code, 200)


class CompanyRegisterViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.country = Country.objects\
                             .get_or_create(name='Test Country', code='xx')[0]
        cls.province = Province.objects.get_or_create(name='Test Province', country=cls.country)[0]
        cls.city = City.objects.get_or_create(name='Test City', province=cls.province)[0]
        user = user_model.objects.create_user('testuser', 'a@b.com', 'testpw')
        UserProfile.objects.create(user=user)

    def setUp(self):
        self.client.login(username='testuser', password='testpw')

    def test_renders_main_template(self):
        response = self.client.get(reverse('company_register'), HTTP_HOST=HOST)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'company_register.html')

    def test_uses_correct_form_class(self):
        response = self.client.get(reverse('company_register'), HTTP_HOST=HOST)

        self.assertIsInstance(response.context['form'], CompanyForm)

    def test_register_company(self):
        data = {
            'name': 'name',
            'legal_name': 'name inc',
            'website': 'company.com',
            'email': 'info@company.com',
            'description': 'We are a good company',
            'cuit': '',
            'phone': '123',  # FIXME: Add validation on phone field
            'country': self.country.id,
            'province': self.province.id,
            'city': self.city.id,
            'contact_name': 'my name',
            'address': 'main street street,',
            'address_number': '451',
            'logo': '',  # FIXME: Path logo here
            'tags': 'tag1,tag2,tag3',
        }
        response = self.client.post(
            reverse('company_register'),
            data=data,
            HTTP_HOST=HOST
        )
        self.assertRedirects(
            response,
            reverse('company_success'),
            status_code=302,
            host=HOST,
            fetch_redirect_response=False
        )


class CompanyProfileTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.country = Country.objects\
                             .get_or_create(name='Test Country', code='xx')[0]
        cls.province = Province.objects.get_or_create(name='Test Province', country=cls.country)[0]
        cls.city = City.objects.get_or_create(name='Test City', province=cls.province)[0]
        user = user_model.objects.create_user('testuser', 'a@b.com', 'testpw')
        cls.company = Company.objects.create(
            name='name',
            legal_name='name inc',
            website='company.com',
            email='info@company.com',
            description='We are a good company',
            phone='123',
            country=cls.country,
            province=cls.province,
            city=cls.city,
            contact_name='my name',
            address='main street street,',
            address_number='451',
            tags='tag1,tag2,tag3',
            active=True
        )
        UserProfile.objects.create(user=user, company=cls.company)

    def setUp(self):
        self.client.login(username='testuser', password='testpw')

    def test_render_profile_template(self):
        response = self.client.get(reverse('company_profile'), HTTP_HOST=HOST)
        self.assertTemplateUsed(response, 'company_profile.html')

    def test_uses_correct_form_class(self):
        response = self.client.get(reverse('company_profile'), HTTP_HOST=HOST)
        self.assertIsInstance(response.context['company_form'], CompanyForm)

    def test_uses_correct_company(self):
        response = self.client.get(reverse('company_profile'), HTTP_HOST=HOST)
        self.assertIsInstance(response.context['company'], Company)
        self.assertEqual(response.context['company'].id, self.company.id)

    def test_update_company_profile(self):
        data = {
            'name': 'name',
            'legal_name': 'name inc',
            'website': 'company.com',
            'email': 'info@company.com',
            'description': 'description updated',
            'cuit': '',
            'phone': '123',  # FIXME: Add validation on phone field
            'country': self.country.id,
            'province': self.province.id,
            'city': self.city.id,
            'contact_name': 'my name',
            'address': 'main street street,',
            'address_number': '451',
            'logo': '',  # FIXME: Path logo here
            'tags': 'tag1,tag2,tag3',
        }

        self.client.post(
            reverse('company_profile'),
            data=data,
            HTTP_HOST=HOST
        )
        self.assertEqual(Company.objects.get(id=self.company.id).description, 'description updated')

    def test_update_company_profile_redirect(self):
        data = {
            'name': 'name',
            'legal_name': 'name inc',
            'website': 'company.com',
            'email': 'info@company.com',
            'description': 'description updated',
            'cuit': '',
            'phone': '123',  # FIXME: Add validation on phone field
            'country': self.country.id,
            'province': self.province.id,
            'city': self.city.id,
            'contact_name': 'my name',
            'address': 'main street street,',
            'address_number': '451',
            'logo': '',  # FIXME: Path logo here
            'tags': 'tag1,tag2,tag3',
        }
        response = self.client.post(
            reverse('company_profile'),
            data=data,
            HTTP_HOST=HOST
        )
        self.assertRedirects(
            response,
            reverse('company_profile'),
            status_code=302,
            host=HOST,
            fetch_redirect_response=False
        )
