from django.core.management.base import BaseCommand

from ...models import Company, Review


class Command(BaseCommand):
    help = ('Calls set_net_rating on all reviews, '
            'then calls set_mark on all active companies')

    def handle(self, *args, **options):
        count = 0
        for review in Review.objects.all():
            review.set_net_rating()
        for company in Company.objects.all():
            count += company.set_mark()
        self.stdout.write('Succesfully set marks for {} companies.'
                          .format(count))
