from django import forms
from django.utils.translation import ugettext_lazy as _

from taggit.forms import TagField
from imagekit.forms import ProcessedImageField
from imagekit.processors import ResizeToFill

from utils.forms import validate_size, val_for
from .models import Company, Country, Province, City, ProductOffering, ServiceOffering


class CompanyForm(forms.ModelForm):

    country = forms.ModelChoiceField(
        label=_('País'),
        empty_label=_('País'),
        queryset=Country.objects.all()
    )
    province = forms.ModelChoiceField(
        label=_('Provincia'),
        empty_label=_('Provincia'),
        queryset=Province.objects.all()
    )
    city = forms.ModelChoiceField(
        label=_('Ciudad'),
        empty_label=_('Ciudad'),
        queryset=City.objects.all()
    )
    tags = TagField(required=False)
    image1 = ProcessedImageField(
        spec_id='companies:company:image1',
        processors=[ResizeToFill(320, 180, upscale=False)],
        format='JPEG',
        validators=[validate_size],
        required=False
    )
    image2 = ProcessedImageField(
        spec_id='companies:company:image2',
        processors=[ResizeToFill(320, 180, upscale=False)],
        format='JPEG',
        validators=[validate_size],
        required=False
    )
    image3 = ProcessedImageField(
        spec_id='companies:company:image3',
        processors=[ResizeToFill(320, 180, upscale=False)],
        format='JPEG',
        validators=[validate_size],
        required=False
    )
    website = forms.URLField(
        widget=forms.TextInput(attrs={'inputmode': 'url'})
    )

    def __init__(self, *args, **kw):
        super(CompanyForm, self).__init__(*args, **kw)
        country_id = val_for(self, 'country')
        self.fields['province'].queryset = Province.objects.filter(
            country__id=country_id
        ).all()
        province_id = val_for(self, 'province')
        self.fields['city'].queryset = City.objects.filter(
            province__id=province_id
        ).all()

    class Meta:
        model = Company
        fields = ('name', 'legal_name', 'website', 'email', 'description',
                  'cuit', 'phone', 'country', 'province', 'city',
                  'contact_name', 'address', 'address_number', 'logo',
                  'image1', 'image2', 'image3', 'tags')


class ProductOfferingForm(forms.ModelForm):
    class Meta:
        model = ProductOffering
        fields = ('product', 'initial_price', 'monthly_price',)


class ServiceOfferingForm(forms.ModelForm):
    class Meta:
        model = ServiceOffering
        fields = ('service', 'initial_price', 'monthly_price',)



class UploadLogoForm(forms.ModelForm):

    logo = ProcessedImageField(spec_id='compdir:company:logo',
                               processors=[ResizeToFill(100, 100)],
                               format='JPEG', validators=[validate_size])

    class Meta:
        model = Company
        fields = ('logo',)
