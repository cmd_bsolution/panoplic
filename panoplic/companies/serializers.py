from rest_framework import serializers

from .models import Company, Product, Service


class CompanySerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='company-detail')
    class Meta:
        fields = ('name', 'email', 'url')
        model = Company


class CompanyInlineSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='company-detail')
    name = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    class Meta:
        fields = ('name', 'email', 'url', 'price')
        model = Company

    def get_name(self, obj):
        return obj.company.name

    def get_email(self, obj):
        return obj.company.email

    def get_price(self, obj):
        return {'initial': obj.initial_price, 'monthly': obj.monthly_price}


class ProductSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='product-detail', lookup_field='code')
    vendors = CompanyInlineSerializer(many=True, source='productoffering_set')

    class Meta:
        fields = ('name', 'code', 'url', 'vendors')
        model = Product
        lookup_field = 'code'
        extra_kwargs = {
            'url': {'lookup_field': 'code'}
        }


class ServiceSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='service-detail', lookup_field='code')
    vendors = CompanyInlineSerializer(many=True, source='serviceoffering_set')

    class Meta:
        fields = ('name', 'code', 'url', 'vendors')
        model = Service
        lookup_field = 'code'
        extra_kwargs = {
            'url': {'lookup_field': 'code'}
        }
