$(function() {

    /* --- Global --- */

    /* Toggle visibility of advanced search fields. */
    $('#toggle-advanced').on('click', function() {
        $('#advanced').toggle();
		$('#search_box').toggleClass('wide');
        
        if ($('#advanced').is(":visible")) {
            $('#toggle-advanced')
                .html('Ocultar <i class="fa fa-chevron-up"></i>');
        } else {
            $('#toggle-advanced')
                .html('Más filtros <i class="fa fa-chevron-right"></i>');
        }
    });

    /* Hide flash messages after 3 seconds */
    $('.flash_msg').delay(3000).fadeOut(200);


    /* --- Homepage (index.html) --- */

    /* Carousel */
    var homeOwl = $("#owl-home");

    homeOwl.owlCarousel({
        items: 4,
        itemsLarge: [2000,4],
        itemsDesktop: [1000,4],
        itemsDesktopSmall: [900,2],
        itemsTablet: [775,2],
        itemsMobile: [479,1]
    });

    // Navigation Events
    $(".next").click(function(){
        homeOwl.trigger('owl.next');
    });
    $(".prev").click(function(){
        homeOwl.trigger('owl.prev');
    });
    $(".play").click(function(){
        homeOwl.trigger('owl.play',1000);
    });


    /* Profile form (profile_edit.html) - datepicker */
    $("#id_birthday").datepicker({ dateFormat: "dd/mm/yy"});


    /* --- Company results (companies.html) --- */
    
    /* Save search */
    $('body')
        .on('click', '#open-save-dialog', function (e) {
            e.preventDefault();
            $('#login_pop_bus').show();
        })
        .on('click', '#close-save-dialog', function (e) {
            e.preventDefault();
            $('#login_pop_bus').hide();
        })
        .on('click', '#save-search', function () {
            saveSearch();
        });
    
    function saveSearch() {
        /* Save current search string, under a name specified in the
        field #nom_busqueda. 
        */
        location.href = "/save_search/" + window.location.search +
        "&name=" + $('#nom_busqueda').val();
    }


    /* --- Avatar (partial/avatar.html) --- */

    $('body')
        .on('change', '#input_avatar', function () {
            showUploading();
            $('#upload_avatar_form').submit();
        })
        .on('click', '#change_avatar', function () {
            $('#input_avatar').show();
        });

    function showUploading() {
        /* Hide avatar upload form and show the "loading" animation. */
        $('#seleccionar_foto').hide();
        $('#uploading').show();
    }


    /* --- Search (partial/search.html) --- */

    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
                currentCategory = "";
            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                self._renderItem(ul, item);
            });
        }
    });

    $("#search_box").catcomplete({
        source: '/search/companies',
        html: true,
        minLength: 3
    });
    $("#zona").autocomplete({
        source: '/search/places'
    });


    /* --- Company profile (company_profile.html) --- */

    $('body')
        .on('click', '.upload_link', function (e) {
            e.preventDefault();
            $($(this).attr('href')).show();                                  
        })

        .on('change', '#input_image1', function () {
            $('#nombre1').html($(this).val());
            $('#imagen1').hide();
            $('#uploading1').show();
        })
        .on('change', '#input_image2', function () {
            $('#nombre2').html($(this).val());
            $('#imagen2').hide();
            $('#uploading2').show();
        })
        .on('change', '#input_image3', function () {
            $('#nombre3').html($(this).val());
            $('#imagen3').hide();
            $('#uploading3').show();
        });


    function getOptions(master, slave) {
        /* Populate a slave select field with options provided by a JSON 
        object. This object is serialized from a Django queryset, filtered by 
        the chosen option of a master select field. 
        */
        var masterModelName = master.attr('name'),
            slaveModelName = slave.attr('name'),
            slaveLabel = slave.children().first().html(),
            newOptions = '<option selected="selected" value="">' + slaveLabel +
                         '</option>';

        $.getJSON(
            '/populate-select/',
            {
                id: master.val(),
                master: masterModelName,
                slave: slaveModelName
            },
            function (data) {  
                var i;

                for (i = 0; i < data.length; i++) {
                    newOptions += '<option value="' + data[i].pk + '">' +
                                   data[i].fields.name + '</option>';
                }
                slave.html(newOptions);
            }
        );
    }

    $('body').on('change', '#id_country', function () {
        getOptions($(this), $('#id_province'));
    });

    $('body').on('change', '#id_province', function () {
        getOptions($(this), $('#id_city'));
    });
});
