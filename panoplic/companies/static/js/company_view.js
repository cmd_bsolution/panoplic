$(function() {

    var CSRFMiddlewareToken = $('input[name=csrfmiddlewaretoken]').val();

    // AJAX request for upvote view
    $('.upvote-form').on('submit', function(e) {
        var form = $(this);

        e.preventDefault();

        $.ajax({
            url: form.attr('action'),
            type: "POST",
            data: {
                button: form.children('.vote-btn').attr('name'),
                csrfmiddlewaretoken: CSRFMiddlewareToken
            },
            success: function(json) {
                if (json.upvoted) {
                    form.children('.vote-btn').attr({name: 'unvoted'})
                                              .html(json.new_btn_value);
                } else {
                    form.children('.vote-btn').attr({name: 'upvoted'})
                                              .html(json.new_btn_value);
                }
                form.nextAll('.vote-count').first().text(json.upvotes);
            },
            error: function(xhr, errmsg, err) {
                console.log(xhr.status + ": " + xhr.responseText);
            },
        });
    });

    // AJAX request for ReviewDeleteView
    var confirmDialog = $('#confirm-delete');

    $('body').on('click', '.review-delete', function(e) {
        var deleteLink = $(this);

        e.preventDefault();
        confirmDialog.show();

        $('#delete-ok').on('click', function() {
            $.ajax({
                url: deleteLink.attr('href'),
                type: "POST",
                data: {
                    csrfmiddlewaretoken: CSRFMiddlewareToken
                },
                success: function(resp) {
                    confirmDialog.hide();
                    deleteLink.parent().html(resp);
                },
                complete: function() {
                    $('#new_review').on('click', function() {
                        location.hash = '#write-review';
                        location.reload();
                    });
                }
            });
        });
    });
    $('#delete-cancel').on('click', function() {
        confirmDialog.hide();
    });

    // AJAX request for ReviewUpdateView
    $('body').on('click', '.review-edit', function(e) {
        var url = $(this).attr('href'),
            reviewDiv = $(this).parents('.review'),
            reviewClone = reviewDiv.clone();

        e.preventDefault();

        // Replace the review's HTML content with the edit form, which is
        // rendered and returned by the Python view.
        $.ajax({
            url: url,
            type: "GET",
            success: function(resp) {
                console.log(resp);
                reviewDiv.html(resp);
                // (re)initialize star rating plugin.
                $('#input-1').rating({
                    min: 0,
                    max: 5,
                    step: 1,
                    glyphicon: false,
                    ratingClass: "rating-fa",
                    size: "xs",
                    starCaptions: {
                        1: "Muy malo",
                        2: "Malo",
                        3: "Regular",
                        4: "Muy bueno",
                        5: "Excelente"
                    },
                    starCaptionClasses: {
                        1: "text-danger",
                        2: "text-warning",
                        3: "text-info",
                        4: "text-primary",
                        5: "text-success"
                    },
                });
            },
        });
        // Cancel: replace the form back with the original html
        reviewDiv.on('click', '#edit-form input[name=cancel]', function() {
            reviewDiv.replaceWith(reviewClone);
        });

        // Hide a picture and flag it for server-side deletion
        reviewDiv.on('click', '.delete-img', function(e) {
            e.preventDefault();
            $(this).parent().hide();
            $(this).next().data('action', 'delete');
        });

        // Flag changed images for a server-side src update and make them
        // visible in the review's HTML
        reviewDiv.on('change', 'input[type=file]', function() {
            $('img#' + $(this).attr('name')).data('action', 'update');
        });

        reviewDiv.on('submit', '#edit-form', function(e) {
            var data = new FormData($(this).get(0)),
                toDelete = '';

            e.preventDefault();

            // Append a string of to-be-deleted image ids to the FormData obj
            $('#edit-form img').filter(function() {
                return $(this).data('action') === 'delete';
            })
                .each(function() {
                    toDelete += ' ' + $(this).attr('id');
                });
            data.append("deletethis", toDelete);

            $.ajax({
                url: url,
                method: "POST",
                data: data,
                success: function(resp) {
                    var updatedReview,
                        updatedReviewImg1,
                        updatedReviewImg2,
                        updatedReviewImg3,
                        i;

                    if (resp.valid) {
                        updatedReview = reviewClone;
                        updatedReviewImg1 = updatedReview.find('.rev-image1');
                        updatedReviewImg2 = updatedReview.find('.rev-image2');
                        updatedReviewImg3 = updatedReview.find('.rev-image3');

                        $('#mean-rating').text(resp.mean_rating);
                        updatedReview.find('.review-rating').text(resp.rating);
                        updatedReview.find('.review-text').text(resp.text);


                        /* Markup for added images.
                        {NUMBER}, {URL} and {CODE} should be replaced by the
                        image number, url and a unique identifier code,
                        respectively. The code is meant for linking the image
                        with its modal dialog. */
                        var imageMarkup = 
                            '<li>\
                                <a href="#" data-toggle="modal" data-target="#modal_image{NUMBER}_{CODE}">\
                                    <img class="rev-image{NUMBER} img-responsive" src="{URL}">\
                                </a>\
                            </li>\
                            <div class="modal fade lightbox" id="modal_image{NUMBER}_{CODE}"\
                                    tabindex="-1" role="dialog" aria-labelledby="review-images-label"\
                                    aria-hidden="true">\
                                <div class="modal-dialog">\
                                    <div class="modal-content">\
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
                                            <span aria-hidden="true">&times;</span>\
                                        </button>\
                                        <img class="rev-image{NUMBER} img-responsive caption" src="{URL}">\
                                    </div>\
                                </div>\
                            </div>';

                        /* Create the imgs if they don't exist or update their 
                        src with a timestamped url if they do, forcing the 
                        browser to refresh them. */

                        // image1
                        if (updatedReviewImg1.length < 1 && resp.image1_url) {
                            updatedReview.find('.review-images')
                                .prepend(
                                    imageMarkup.replace(/{NUMBER}/g, '1')
                                               .replace(/{URL}/g, resp.image1_url)
                                               .replace(/{CODE}/g, 'rep1')
                                );
                        } else if (resp.image1_url) {
                            updatedReviewImg1.attr('src', resp.image1_url);
                            updatedReviewImg1.closest('li')
                                             .css('display', 'inline-block');
                        }
                        // image2
                        if (updatedReviewImg2.length < 1 && resp.image2_url) {
                            // Middle image - place it before the last one if 
                            // that one exists, or else append it to the
                            // container.
                            if (updatedReviewImg3.length > 0) {
                                updatedReview.find('img.rev-image3')
                                    .closest('li')
                                    .before(
                                        imageMarkup.replace(/{NUMBER}/g, '2')
                                                   .replace(/{URL}/g, resp.image2_url)
                                                   .replace(/{CODE}/g, 'rep2')
                                    );
                            } else {
                                updatedReview.find('.review-images')
                                    .append(
                                        imageMarkup.replace(/{NUMBER}/g, '2')
                                                   .replace(/{URL}/g, resp.image2_url)
                                                   .replace(/{CODE}/g, 'rep2')
                                    );
                            }
                        } else if (resp.image2_url) {
                            updatedReviewImg2.attr('src', resp.image2_url);
                            updatedReviewImg2.closest('li')
                                             .css('display', 'inline-block');
                        }
                        // image3
                        if (updatedReviewImg3.length < 1 && resp.image3_url) {
                            updatedReview.find('.review-images')
                                .append(
                                    imageMarkup.replace(/{NUMBER}/g, '3')
                                               .replace(/{URL}/g, resp.image3_url)
                                               .replace(/{CODE}/g, 'rep3')
                                );
                        } else if (resp.image3_url) {
                            updatedReviewImg3.attr('src', resp.image3_url);
                            updatedReviewImg3.closest('li')
                                             .css('display', 'inline-block');
                        }

                        // Hide deleted images
                        for (i = 0; i < resp.deleted_images.length; i++) {
                            updatedReview
                                .find('.rev-' + resp.deleted_images[i])
                                .closest('li')
                                .css('display', 'none');
                        }

                        reviewDiv.replaceWith(updatedReview);
                    } else {
                        reviewDiv.html(resp);
                    }
                },
                processData: false,
                contentType: false
            });
        });
    });

    // AJAX request for ReportCompany view
    $('body').on('click', '#report-comp-btn', function() {
        var reportButton = $(this),
            reportForm = reportButton.next();

        reportButton.hide();
        reportForm.show();

        reportForm.on('click', 'button.report-close', function() {
            reportForm.hide();
            reportButton.show();
        });

        reportForm.on('change', 'select', function() {
            reportForm.find('div').show();
        });

        reportForm.on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: {
                    motive: reportForm.find('select').val(),
                    text: reportForm.find('textarea').val(),
                    csrfmiddlewaretoken: CSRFMiddlewareToken
                },
                success: function(resp) {
                    reportForm.replaceWith('<p>' + resp.message + '</p>');
                },
            });
        });
    });

    // AJAX request for ReportReview view
    $('body').on('click', '.report-review-btn', function() {
        var reportButton = $(this),
            reportForm = reportButton.next();

        reportButton.hide();
        reportForm.show();

        reportForm.on('click', 'button.report-close', function() {
            reportForm.hide();
            reportButton.show();
        });

        reportForm.on('change', 'select', function() {
            reportForm.find('div').show();
        });

        reportForm.on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: {
                    motive: reportForm.find('select').val(),
                    text: reportForm.find('textarea').val(),
                    csrfmiddlewaretoken: CSRFMiddlewareToken
                },
                success: function(resp) {
                    reportForm.replaceWith('<p>' + resp.message + '</p>');
                },
            });
        });
    });

    $('body').on('click', '.desplegables', function () {
        $(this).children('i').toggleClass('fa-chevron-right fa-chevron-down');
        $(this).parent().next('div').toggle();
    });
});
