import csv

from django.contrib import admin
from django.http import HttpResponse
from django.template.defaultfilters import slugify

from .models import (
    Country, Province, City, Category, Product, Service, Company,
    ProductOffering, ServiceOffering
)


def make_published(modeladmin, request, queryset):
    queryset.update(active=True)
make_published.short_description = "Mark selected stories as published"


def make_company_published(modeladmin, request, queryset):
    for c in queryset.all():
        c.active = True
        c.save()
make_company_published.short_description = make_published.short_description


def export_as_csv(modeladmin, request, queryset):
    model = queryset.model
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = ('attachment; filename=%s.csv'
                                       % slugify(model.__name__))
    writer = csv.writer(response)
    headers = []
    for field in model._meta.fields:
        headers.append(field.name)
    writer.writerow(headers)
    # Write data to CSV file
    for obj in queryset:
        row = []
        for field in headers:
            if field in headers:
                val = getattr(obj, field)
                if callable(val):
                    val = val()
                if type(val) == str:
                    val = val.encode("utf-8")
                row.append(val)
        writer.writerow(row)
    # Return CSV file to browser as download
    return response
export_as_csv.short_description = "Export selected to CSV file"


class ServiceOfferingAdmin(admin.TabularInline):
    # list_display = ('company', 'service', 'initial_price', 'monthly_price')
    model = ServiceOffering


class ProductOfferingAdmin(admin.TabularInline):
    # list_display = ('company', 'product', 'initial_price', 'monthly_price')
    model = ProductOffering


class CompanyAdmin(admin.ModelAdmin):

    list_filter = ('active',)
    actions = [make_company_published, export_as_csv]
    filter_horizontal = ('products', 'services')
    inlines = [ProductOfferingAdmin, ServiceOfferingAdmin]

    def get_queryset(self, request):
        return Company.objects.all_include_inactive()


class CityAdmin(admin.ModelAdmin):
    list_filter = ('province',)


class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {"code": ("name",)}
    inlines = [ProductOfferingAdmin]


class ServiceAdmin(admin.ModelAdmin):
    prepopulated_fields = {"code": ("name",)}
    inlines = [ServiceOfferingAdmin]


admin.site.register(Company, CompanyAdmin)
admin.site.register(Country)
admin.site.register(Province)
admin.site.register(City, CityAdmin)
admin.site.register(Category)
admin.site.register(Product, ProductAdmin)
admin.site.register(Service, ServiceAdmin)
