from django.apps.registry import apps
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext as _
from django.views.generic.edit import FormMixin
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.list import BaseListView, ListView

from reports.forms import CompanyReportForm, ReviewReportForm
from reviews.models import Upvote
from reviews.forms import ReviewForm, UpvoteForm
from reviews.views import UPVOTE_TEXT, DOWNVOTE_TEXT
from users.models import UserProfile
from .forms import CompanyForm, UploadLogoForm
from .models import Company


class CompanyListView(ListView):

    template_name = 'companies.html'
    paginate_by = 10

    def get_queryset(self):
        """Display active companies"""
        return Company.objects.filter(country__code=self.request.subdomain)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['companies'] = context['page_obj']
        return context


class CompanyView(TemplateResponseMixin, FormMixin, BaseListView):
    """
    Render company details and associated reviews
    [and create a new review on POST].
    """

    SORTING_OPTIONS = {
        'date': {'ascending': 'date-asc', 'descending': 'date-desc'},
        'upvotes': {'ascending': 'upvotes-asc', 'descending': 'upvotes-desc'},
        'rating': {'ascending': 'rating-asc', 'descending': 'rating-desc'}
    }

    template_name = 'company_view.html'
    context_object_name = 'review_list'
    paginate_by = 10
    form_class = ReviewForm

    def dispatch(self, request, *args, **kwargs):
        """
        If the `slug` part of the URL doesn't correspond to the name of the
        company, replace it with one that does by performing a redirect.
        """
        self.company = get_object_or_404(Company, pk=self.kwargs['pk'])
        if slugify(self.company.name) != self.kwargs.get('slug'):
            return redirect(self.company, permanent=True)
        return super(CompanyView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(CompanyView, self).get_form_kwargs()
        kwargs.update(company=self.company, user=self.request.user)
        return kwargs

    def form_invalid(self, form):
        self.sort_param = 'date-desc'
        self.object_list = self.ordered(self.company.review_set.all())
        self._flag_upvoted_reviews(self.object_list)

        context = self.get_context_data(review_form=form)
        context['reviews_page'] = context['page_obj']
        return self.render_to_response(context)

    def form_valid(self, form):
        review = form.save()
        review.send_notification()
        self.company.set_mean_rating()
        return HttpResponseRedirect(
            reverse('company_view',
                    args=(self.company.pk, slugify(self.company.name)))
            + '#arrange'
        )

    def get(self, request, *args, **kwargs):
        self.sort_param = self.request.GET.get('rsort', 'date-desc')
        self.object_list = self.ordered(self.company.review_set.all())

        if request.user.is_authenticated():
            self._flag_upvoted_reviews(self.object_list)

        context = self.get_context_data()
        context['reviews_page'] = context['page_obj']
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = {
            'company': self.company,
            'encoded_address': self.get_encoded_address(self.company),
            'review_form': kwargs.get('review_form') or ReviewForm(company=self.company, user=self.request.user),
            'upvote_form': UpvoteForm(),
            'company_report_form': CompanyReportForm(),
            'review_report_form': ReviewReportForm(),

            # text for upvote/downvote buttons is passed as context to
            # also make it available as JavaScript variables.
            'upvote_text': UPVOTE_TEXT,
            'downvote_text': DOWNVOTE_TEXT,

            # values for constructing the querystring in sorting links
            'by_date':
                self._filter_options(self.SORTING_OPTIONS['date']),
            'by_upvotes':
                self._filter_options(self.SORTING_OPTIONS['upvotes']),
            'by_rating':
                self._filter_options(self.SORTING_OPTIONS['rating'])
        }
        return super().get_context_data(**context)

    def get_encoded_address(self, company):
        """
        Encode the company's address in a format suitable for Google Maps.
        """
        encoded_address = self.company.address +\
            (' ' + company.address_number if company.address_number else '') +\
            (',' + company.city.name if company.city else '') +\
            (',' + company.province.name if company.province else '')
        return encoded_address

    def ordered(self, object_list):
        """
        Sort queryset results by criteria provided via querystring parameter.
        """
        if self.sort_param:
            options = self.SORTING_OPTIONS
            criteria = {
                options['date']['ascending']: ('created_on',),
                options['date']['descending']: ('-created_on',),
                options['upvotes']['ascending']: ('upvotes', '-created_on'),
                options['upvotes']['descending']: ('-upvotes', '-created_on'),
                options['rating']['ascending']: ('rating', '-created_on'),
                options['rating']['descending']: ('-rating', '-created_on')
            }
            object_list = object_list.order_by(*criteria[self.sort_param])
        return object_list

    def post(self, request, *args, **kwargs):
        """Handle POST requests."""
        if not request.user.is_authenticated():
            return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
        form = self.get_form(ReviewForm)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def _flag_upvoted_reviews(self, review_list):
        """
        Set each review's has_user_vote attribute to True or False depending
        on whether or not the requesting user has already upvoted it.
        """
        for review in review_list:
            review.has_user_vote = \
                Upvote.objects\
                      .filter(user=self.request.user, review=review)\
                      .exists()

    def _filter_options(self, options):
        """
        Return the default descending version of a sorting option, or its
        ascending counterpart in case the former is the current value of
        the sorting GET parameter.
        """
        if options['descending'] == self.sort_param:
            return options['ascending']
        else:
            return options['descending']


@login_required
def company_register(request):
    """
    This view serves and validates a company registration form.
    """
    profile = request.user.profile
    if profile.company:
        return HttpResponseRedirect(reverse('company_profile'))

    if request.method == 'POST':
        form = CompanyForm(request.POST)
        if form.is_valid():
            company = form.save()
            profile.company = company
            profile.save()
            company.send_creation_email(request)
            return HttpResponseRedirect(reverse('company_success'))
    else:
        form = CompanyForm()

    return render(request, 'company_register.html', {'form': form})


@login_required
def company_profile(request):
    """
    This view serves and validates a company profile form.
    If also encodes the address to display a google map.
    """
    profile = UserProfile.objects.get(user__pk=request.user.id)
    company = profile.company

    if request.method == 'POST':
        company_form = CompanyForm(request.POST, request.FILES,
                                   instance=company)

        if company_form.is_valid():
            company_form.save()
            messages.add_message(request, messages.INFO, _('Company saved.'))
            return HttpResponseRedirect(reverse('company_profile'))
        else:
            company_form = CompanyForm(request.POST, instance=company)
    else:
        company_form = CompanyForm(instance=company)

    try:
        area = company.city.name + "," + company.province.name
    except AttributeError:
        try:
            area = company.province.name
        except AttributeError:
            area = company.country.name
    address = ','.join((company.address + ' ' + company.address_number, area))
    return render(request, 'company_profile.html',
                  {'company_form': company_form, 'encoded_address': address,
                   'company': company})


def upload_logo(request):
    """
    This view handles the logo upload request.
    """
    profile = UserProfile.objects.get(user__pk=request.user.id)
    company = profile.company
    if request.method == 'POST':
        form = UploadLogoForm(request.POST, request.FILES, instance=company)
        if form.is_valid():
            form.save()
        else:
            [messages.add_message(request, messages.ERROR, error.as_text()) for field,error in form.errors.items()]
    return HttpResponseRedirect(reverse('company_profile'))


def delete_logo(request):
    """
    This view handles the logo deletion request.
    """
    profile = UserProfile.objects.get(user__pk=request.user.id)
    company = profile.company
    company.logo = company.logo.field.default
    company.save()
    return HttpResponseRedirect(reverse('company_profile'))


def populate_select(request):
    """
    Build a queryset from a model name and a filter derived from GET
    parameters, and return it as a JSON serialized strting in an HTTP
    response.

    Called via AJAX to populate a ModelChoiceFields with options based on a
    previous selection.
    """

    master_model = request.GET["master"]
    master_id = request.GET["id"]
    slave_model = request.GET["slave"].capitalize()
    filter_param = {master_model + '__id': master_id}
    options = apps.get_model('companies', slave_model).objects\
                                                      .filter(**filter_param)
    data = serializers.serialize('json', options)
    return HttpResponse(data, content_type="application/json")
