from django import template

register = template.Library()


@register.assignment_tag
def limit_range(page_obj, distance):
    """
    Return a generator for a subset of page numbers in a Paginator object,
    within a certain distance from the number of the passed in Page.
    """
    start = max(page_obj.number - distance - 1, 0)
    stop = page_obj.number + distance
    limited_range = page_obj.paginator.page_range[start:stop]
    return (i for i in limited_range)


@register.simple_tag(takes_context=True)
def add_query(context, **kwargs):
    """Add query string parameters to the current URL."""
    request = context.get('request')
    qs_dict = request.GET.copy()
    qs_dict.update(kwargs)
    querystring = '&'.join("{}={}".format(k, v) for k, v in qs_dict.items())
    return request.path + '?' + querystring
