"""
Legacy custom tags.
"""

from django import template
from django import forms
from django.forms.forms import NON_FIELD_ERRORS
from django.forms.utils import ErrorDict
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _


register = template.Library()

@register.filter
def nice_errors(form, non_field_msg=_('General form errors')):
    nice_errors = ErrorDict()
    nice_errors['non_field_errors'] = ErrorDict()
    nice_errors['field_errors'] = ErrorDict()
    for field, errors in form.errors.items():
        if field == NON_FIELD_ERRORS:
            dict = nice_errors['non_field_errors']
            key = non_field_msg
        else:
            dict = nice_errors['field_errors']
            key = form.fields[field].label
        if len(errors) != 0:
            dict[key] = errors
    return nice_errors

@register.filter
def merge_errors(form1, form2):
    class mock_form(object):
        errors = dict()
        fields = dict()
    if (not isinstance(form1, forms.BaseForm)) or (not isinstance(form2, forms.BaseForm)):
        return
    form = mock_form()
    for field, errors in form1.errors.items():
        form.errors[field] = errors
        if field != NON_FIELD_ERRORS:
            form.fields[field] = form1.fields[field]
    for field, errors in form2.errors.items():
        if not field in form.errors:
            form.errors[field] = errors
            if field != NON_FIELD_ERRORS:
                form.fields[field] = form2.fields[field]
        else:
            for error in errors:
                if form.errors[field].count(error) == 0:
                    form.errors[field].push(error)
    return form

@register.filter
def truncatechars(value, arg):
    """Truncate the text when it exceeds a certain number of characters.
    Delete the last word only if partial.
    Adds '...' at the end of the text.
    
    Example:
    
        {{ text|truncatewords_by_chars:25 }}
    """
    try:
        length = int(arg)
    except ValueError:
        return value
    
    if len(value) > length:
        if value[length:length + 1].isspace():
            return value[:length].rstrip() + '...'
        else:
            return value[:length].rsplit(' ', 1)[0].rstrip() + '...'
    else:
        return value
  
    
@register.simple_tag(takes_context=True)    
def favorite(context, company):
    if context['user'].is_authenticated():
        if company in context['user'].profile.favorites.all():
            url = reverse('unfavorite', args=[company.id])
            text = "Favorito"
        else:
            url = reverse('favorite', args=[company.id])
            text = "Agregar a favoritos"
        return '<a href="{}"><button class="btn favoritebtn">\
            <i class="fa fa-heart"></i> {}</button></a>'.format(url, text)
    else:
        return ''

   
@register.filter
def add_get_parameter(url, param):
    key, value = param.split('=')
    if key in url:
        p = url[url.index(key):]
        return url.replace(p, param)
    else:
        return url + ('&' if '/?' in url else '?') + param


@register.filter
def remove_http(url):
    if url[:7] == "http://":
        url = url[7:]
    return url

@register.simple_tag
def sitedomain():
    '''Returns the URL of the default site.'''
    try:
        return Site.objects.get_current().domain
    except Site.DoesNotExist:
        return None

@register.inclusion_tag('partial/labeled_field.html')
def labeled(field):
    return { 'field' : field }
