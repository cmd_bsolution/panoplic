from django.contrib import sitemaps

from .models import Company


class CompanySitemap(sitemaps.Sitemap):

    limit = 1000
    changefreq = "yearly"
    priority = 1.0

    def items(self):
        return Company.objects.all()
