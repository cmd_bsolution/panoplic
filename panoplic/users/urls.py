from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^profile/(?P<pk>\d+)/$',
        views.ProfileView.as_view(),
        name='profile'
    ),
    url(
        r'^profile/edit/$',
        views.profile_edit,
        name='edit_profile'
    ),
    url(
        r'^upload_avatar/$',
        views.upload_avatar,
        name='upload_avatar'
    ),
    url(
        r'^delete_avatar/$',
        views.delete_avatar,
        name='delete_avatar'
    ),
    url(
        r'^favorite/(?P<id>\d+)/$',
        views.favorite,
        name='favorite'
    ),
    url(
        r'^unfavorite/(?P<id>\d+)/$',
        views.unfavorite,
        name='unfavorite'
    ),
    url(
        r'^save_search/$',
        views.save_search,
        name='save_search'
    ),
    url(
        r'^remove_search/(?P<id>\d+)/$',
        views.remove_search,
        name='remove_search'
    ),
]
