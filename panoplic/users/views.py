from django.conf import settings
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.contrib.sites.requests import RequestSite
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template.loader import render_to_string
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext as _
from django.views.generic import DetailView, FormView

from registration import signals
from registration.models import RegistrationProfile
from registration.views import RegistrationView

from companies.models import Company
from search.models import Search
from search.forms import SearchForm
from utils.views import LoginRequiredMixin
from .models import UserProfile
from .forms import (
    ProfileForm, UserForm, CustomRegistrationForm, UploadAvatarForm,
    MinLengthPasswordChangeForm
)


class CustomRegistrationView(RegistrationView):

    form_class = CustomRegistrationForm

    def register(self, form):
        username = form.cleaned_data['username']
        email = form.cleaned_data['email']
        password = form.cleaned_data['password1']

        if Site._meta.installed:
            site = Site.objects.get_current()
        else:
            site = RequestSite(self.request)

        user = RegistrationProfile.objects.create_inactive_user(
            site,
            send_email=False,
            username=username,
            email=email,
            password=password,
        )
        signals.user_registered.send(sender=self.__class__, user=user,
                                     request=self.request)

        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user.save()

        profile = UserProfile(user=user)
        profile.country = form.cleaned_data['country']
        profile.province = form.cleaned_data['province']
        profile.save()

        self.send_activation_email(site, user)

    def send_activation_email(self, site, user):
        """Send the activation mail"""
        profile = RegistrationProfile.objects.get(user=user)
        context_dict = {
            'activation_key': profile.activation_key,
            'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
            'site': site,
            'user': user,
            'STATIC_URL': settings.STATIC_URL,
        }
        message = render_to_string('email_templates/activation_email.txt', context_dict)
        html_message = render_to_string('email_templates/activation_email.html', context_dict)
        send_mail(
            'Activar su cuenta',
            message=message,
            html_message=html_message,
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[user.email],
            fail_silently=True,
        )

    def get_success_url(self, user=None):
        """
        Return the name of the URL to redirect to after successful
        user registration.
        """
        return ('registration_complete', (), {})

    def registration_allowed(self):
        return not self.request.user.is_authenticated()


class ProfileView(DetailView):
    """Display the user's profile."""

    model = UserProfile
    context_object_name = 'profile'
    template_name = 'profile.html'


@login_required
def profile_edit(request):
    """This view serves and validates a user profile form."""
    profile = request.user.profile

    if request.method == 'POST':
        profile_form = ProfileForm(request.POST, instance=profile)
        user_form = UserForm(request.POST, instance=profile.user)
        if profile_form.is_valid() and user_form.is_valid():
            profile_form.save()
            user_form.save()
            messages.add_message(request, messages.INFO, _('Profile saved.'))
    else:
        profile_form = ProfileForm(instance=profile)
        user_form = UserForm(instance=request.user)

    return render(request, 'profile_edit.html', {
        'profile_form': profile_form,
        'user_form': user_form,
        'avatar': profile.avatar,
        'favorites': profile.favorites,
        'searches': profile.searches
    })


@login_required
def save_search(request):
    """
    Save a search into the user's profile.
    """
    form = SearchForm(request.GET)
    if form.is_valid():
        search = form.save()
        profile = UserProfile.objects.get(user=request.user)
        profile.searches.add(search)
        profile.save()
        messages.add_message(request, messages.INFO, _('Search saved.'))
        return HttpResponseRedirect(search.get_absolute_url())


def remove_search(request, id):
    """
    Remove a search from the user's profile.
    """
    profile = UserProfile.objects.get(user__pk=request.user.id)
    search = Search.objects.get(pk=id)
    profile.searches.remove(search)
    return HttpResponseRedirect(reverse('edit_profile'))


def favorite(request, id):
    """
    This view adds a company to user favorites.
    """
    profile = UserProfile.objects.get(user__pk=request.user.id)
    company = Company.objects.get(pk=id)
    profile.favorites.add(company)
    return HttpResponseRedirect(reverse('company_view', args=(company.id, slugify(company.name))))


def unfavorite(request, id):
    """
    This view removes a company from user favorites.
    """
    profile = UserProfile.objects.get(user__pk=request.user.id)
    company = Company.objects.get(pk=id)
    profile.favorites.remove(company)
    return HttpResponseRedirect(reverse('profile'))


class PasswordChangeView(LoginRequiredMixin, FormView):

    template_name = 'registration/password_change.html'
    form_class = MinLengthPasswordChangeForm

    def get_success_url(self):
        return reverse('profile', args=(self.request.user.pk,))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        messages.success(self.request,
                         _("Su contraseña ha sido cambiada exitosamente."))
        update_session_auth_hash(self.request, form.user)
        return super().form_valid(form)


def upload_avatar(request):
    """
    This view handles the avatar upload request.
    """
    profile = UserProfile.objects.get(user__pk=request.user.id)
    if request.method == 'POST':
        form = UploadAvatarForm(request.POST, request.FILES, instance=profile)
        if form.is_valid():
            form.save()
        else:
            [messages.add_message(request, messages.ERROR, error.as_text()) for
             field, error in form.errors.items()]
    return HttpResponseRedirect(reverse('edit_profile'))


def delete_avatar(request):
    """
    This view handles the avatar deletion request.
    """
    profile = UserProfile.objects.get(user__pk=request.user.id)
    profile.avatar = profile.avatar.field.default
    profile.save()
    return HttpResponseRedirect(reverse('edit_profile'))
