from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.core.validators import MinLengthValidator
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _

from imagekit.forms import ProcessedImageField
from imagekit.processors import ResizeToFill
from registration.forms import RegistrationFormUniqueEmail

from utils.forms import validate_size, raw_value, val_for
from companies.models import Company, City, Province, Country
from .models import UserProfile


validate_password = MinLengthValidator(
    settings.MIN_PASSWORD_LENGTH,
    _("La contraseña debe tener al menos {} caracteres"
      .format(settings.MIN_PASSWORD_LENGTH))
)


class CustomRegistrationForm(RegistrationFormUniqueEmail):
    """
    A registration form that only requires the user to enter their e-mail
    address and password. The username is automatically generated
    This class requires django-registration to extend the
    RegistrationFormUniqueEmail
    """
    MIN_PASSWORD_LENGTH = 8

    username = forms.CharField(widget=forms.HiddenInput, required=False)
    password1 = forms.CharField(
        label=_('Contraseña'),
        widget=forms.PasswordInput,
        validators=[validate_password]
    )
    first_name = forms.CharField(label=_('Nombre'), )
    last_name = forms.CharField(label=_('Apellido'))
    country = forms.ModelChoiceField(
        label=_('País'),
        empty_label=_(u'País'),
        queryset=Country.objects.order_by('name')
    )
    province = forms.ModelChoiceField(
        label=_('Provincia'),
        empty_label=_('Provincia'),
        queryset=None
    )
    city = forms.ModelChoiceField(
        label=_('Ciudad'),
        empty_label=_('Ciudad'),
        queryset=None
    )

    def __init__(self, *args, **kw):
        super(RegistrationFormUniqueEmail, self).__init__(*args, **kw)
        self.fields.keyOrder = [
            'first_name',
            'last_name',
            'country',
            'city',
            'province',
            'email',
            'password1',
            'password2',
        ]
        self.fields['province'].queryset = Province.objects.filter(
            country__id=self.val_for('country')
        ).all()
        self.fields['city'].queryset = City.objects.filter(
            province__id=self.val_for('province')
        ).all()

    def clean(self):
        if not self.errors:
            self.cleaned_data['username'] = self.generate_username(
                self.cleaned_data['email']
            )
        super(CustomRegistrationForm, self).clean()
        return self.cleaned_data

    def clean_username(self):
        """
        This function is required to overwrite an inherited username clean.
        """
        return self.cleaned_data['username']

    def val_for(self, field_name):
        return raw_value(self, field_name) or None

    def generate_username(self, email):
        localpart = email.split('@', 1)[0][:25]
        c = get_user_model().objects.filter(username__startswith=localpart).count()
        if c > 0:
            localpart += str(c + 1)
        return localpart


class MinLengthPasswordChangeForm(PasswordChangeForm):

    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput,
        validators=[validate_password]
    )


class CustomAuthenticationForm(AuthenticationForm):
    username = forms.EmailField(label=_("Email"))


class UserForm(ModelForm):
    email = forms.EmailField(label=_(u'Email'), required=True)

    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'email')


class ProfileForm(ModelForm):
    country = forms.ModelChoiceField(
        label=_(u'Country'),
        empty_label=_(u'Country'),
        queryset=Country.objects.all(),
    )
    province = forms.ModelChoiceField(
        label=_(u'Province'),
        empty_label=_(u'Province'),
        queryset=Province.objects.all()
    )
    city = forms.ModelChoiceField(
        label=_(u'City'),
        empty_label=_(u'City'),
        queryset=None
    )
    birthday = forms.DateField(
        ('%d/%m/%Y', '%Y-%m-%d'),
        widget=forms.DateInput(attrs={'autocomplete': 'off'}),
        label=_('Birthday')
    )

    def __init__(self, *args, **kw):
        super(ProfileForm, self).__init__(*args, **kw)
        country_id = val_for(self, 'country')
        self.fields['province'].queryset = Province.objects.filter(
            country__id=country_id
        ).all()
        province_id = val_for(self, 'province')
        self.fields['city'].queryset = City.objects.filter(
            province__id=province_id
        ).all()

    class Meta:
        model = UserProfile
        fields = ('city', 'province', 'country', 'birthday', 'gender', 'phone',
                  'avatar')


class UploadAvatarForm(ModelForm):

    avatar = ProcessedImageField(spec_id='users:userprofile:avatar',
                                 processors=[ResizeToFill(100, 100)],
                                 format='JPEG', validators=[validate_size])

    class Meta:
        model = UserProfile
        fields = ('avatar',)


class UploadLogoForm(ModelForm):

    logo = ProcessedImageField(spec_id='companies:company:logo',
                               processors=[ResizeToFill(100, 100)],
                               format='JPEG', validators=[validate_size])

    class Meta:
        model = Company
        fields = ('logo',)
