from tempfile import NamedTemporaryFile

import requests

from django.core.files import File

from .models import UserProfile


def save_profile(backend, user, response, details, uid, *args, **kwargs):
    """
    Create a UserProfile for the user based on data from the provider.
    """
    prof = UserProfile.objects.get_or_create(user=user)[0]

    # Update avatar from profile image
    if backend.name == 'facebook':
        picture_data = requests.get(
            "https://graph.facebook.com/{}/picture".format(uid),
            params={
                'redirect': 'false',
                'type': 'large',
                'width': 100,
                'height': 100
            }
        ).json()['data']
        image_url = picture_data['url']
        is_default = picture_data['is_silhouette']
    else:
        image_url = None

    if image_url and not is_default:
        with NamedTemporaryFile() as temp_image:
            temp_image.write(requests.get(image_url).content)
            prof.avatar.save("{}".format(user), File(temp_image))
