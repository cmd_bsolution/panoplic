import os

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from panoplic.storage import OverwriteStorage
from companies.models import Company, City, Province, Country
from search.models import Search


class User(AbstractUser):
    pass


def avatar_path(instance, filename):
    return os.path.join('avatars', instance.user.username)


class UserProfile(models.Model):

    GENDER_CHOICES = (('M', _('Hombre')), ('F', _('Mujer')))

    user = models.OneToOneField(User, related_name='profile')
    city = models.ForeignKey(City, null=True)
    province = models.ForeignKey(Province, null=True)
    country = models.ForeignKey(Country, null=True)
    birthday = models.DateField(
        blank=True,
        null=True,
        verbose_name=_('Birthday')
    )
    gender = models.CharField(
        max_length=3,
        choices=GENDER_CHOICES,
        blank=True,
        verbose_name=_('Gender')
    )
    phone = models.CharField(
        max_length=50,
        blank=True,
        verbose_name=_('Phone')
    )
    favorites = models.ManyToManyField(
        Company,
        related_name='favorites',
        blank=True
    )
    searches = models.ManyToManyField(Search, blank=True)
    company = models.ForeignKey(
        Company,
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )
    notify_security = models.BooleanField(default=False)
    notify_video = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to=avatar_path,
                               default='avatars/default.gif',
                               storage=OverwriteStorage())

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')

    def get_rank(self):
        reviews = self.user.review_set.count()
        if reviews >= 3 and reviews < 6:
            return _("Crítico novato")
        elif reviews >= 6 and reviews < 11:
            return _("Crítico intermedio")
        elif reviews >= 11 and reviews < 21:
            return _("Crítico Avanzado")
        elif reviews >= 21 and reviews < 50:
            return _("Crítico Experto")
        elif reviews > 50:
            return _("Colaborador")

    def __str__(self):
        return self.user.username
