from django.contrib import admin

from .models import UserProfile


class DeleteNotAllowedModelAdmin(admin.ModelAdmin):
    # remove delete action on list page
    def get_actions(self, request):
        actions = super(DeleteNotAllowedModelAdmin, self).get_actions(request)
        try:
            del actions['delete_selected']
        except KeyError:
            pass
        return actions
    # remove delete link on entity page

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(UserProfile, DeleteNotAllowedModelAdmin)
