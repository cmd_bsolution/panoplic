from urllib.parse import urlparse

from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase

from companies.tests.factories import (
    CountryFactory, ProvinceFactory, CityFactory, ProductFactory,
    CategoryFactory, ServiceFactory
)
from ..forms import UserForm, ProfileForm, CustomRegistrationForm
from ..models import UserProfile


HOST = 'test.panoplic-dev.com'
user_model = get_user_model()


class ProfileEditViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.username = 'testuser'
        cls.email = 'a@b.com'
        cls.password = 'testpw'
        cls.user = user_model.objects.create_user(cls.username, cls.email, cls.password)
        UserProfile.objects.create(user=cls.user)

    def setUp(self):
        self.client.login(username=self.username, password=self.password)

    def test_renders_main_template(self):
        response = self.client.get(reverse('edit_profile'), HTTP_HOST=HOST)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'profile_edit.html')

    def test_uses_correct_form_class(self):
        response = self.client.get(reverse('edit_profile'), HTTP_HOST=HOST)

        self.assertIsInstance(response.context['profile_form'], ProfileForm)
        self.assertIsInstance(response.context['user_form'], UserForm)

    def test_update(self):
        first_name = 'custom_first_name'
        country = CountryFactory.create()
        province = ProvinceFactory.create(country=country)
        city = CityFactory.create(province=province)
        data = {
            'email': self.email,
            'username': self.username,
            'first_name': first_name,
            'country': country.id,
            'province': province.id,
            'city': city.id,
            'birthday': '2000-1-1',
        }
        self.client.post(
            reverse('edit_profile'),
            data=data,
            HTTP_HOST=HOST
        )
        refreshed_user = user_model.objects.get(email=self.email)
        self.assertEqual(refreshed_user.first_name, first_name)
        self.assertEqual(refreshed_user.profile.country, country)


class RegistrationViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.email = 'email@example.com'
        cls.password = 'password'
        cls.country = CountryFactory.create()
        cls.province = ProvinceFactory.create(country=cls.country)
        cls.city = CityFactory.create(province=cls.province)

    def test_renders_main_template(self):
        response = self.client.get(reverse('registration_register'),
                                   HTTP_HOST=HOST)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'registration/registration_form.html')

    def test_uses_correct_form_class(self):
        response = self.client.get(reverse('registration_register'),
                                   HTTP_HOST=HOST)

        self.assertIsInstance(response.context['form'], CustomRegistrationForm)

    def test_register_valid_user(self):
        data = {
            'first_name': 'first name',
            'last_name': 'last name',
            'country': self.country.id,
            'city': self.city.id,
            'province': self.province.id,
            'email': self.email,
            'password1': self.password,
            'password2': self.password,
        }
        response = self.client.post(
            reverse('registration_register'),
            data=data,
            HTTP_HOST=HOST
        )
        self.assertEqual(response.status_code, 302)
        self.assertTrue(user_model.objects.filter(email=self.email).exists())

    def test_login_after_registration(self):
        data = {
            'first_name': 'first name',
            'last_name': 'last name',
            'country': self.country.id,
            'city': self.city.id,
            'province': self.province.id,
            'email': self.email,
            'password1': self.password,
            'password2': self.password,
        }
        response = self.client.post(
            reverse('registration_register'),
            data=data,
            HTTP_HOST=HOST
        )

        # Activate user!
        user = user_model.objects.get(email=self.email)
        user.is_active = True
        user.save()

        response = self.client.post(
            reverse('auth_login'),
            data={
                'username': self.email,
                'password': self.password
            },
            HTTP_HOST=HOST
        )
        self.assertRedirects(
            response,
            reverse('home'),
            status_code=302,
            host=HOST,
            fetch_redirect_response=False
        )


class SearchSaveTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.username = 'testuser'
        cls.email = 'a@b.com'
        cls.password = 'testpw'
        user = user_model.objects.create_user(cls.username, cls.email, cls.password)
        UserProfile.objects.create(user=user)

    def setUp(self):
        self.client.login(username=self.username, password=self.password)

    def test_save(self):
        category = CategoryFactory.create()
        product = ProductFactory.create()
        service = ServiceFactory.create()

        response = self.client.get(
            reverse('save_search'),
            data={
                'name': '',
                'companies_text': 'alarmas',
                'places_text': '',
                'category': category.id,
                'product': product.id,
                'service': service.id,
            },
            HTTP_HOST=HOST
        )
        self.assertEqual(response.status_code, 302)
        target_url = urlparse(response.url)
        query_params = dict(q.split('=') for q in target_url.query.split('&'))
        self.assertEqual(query_params['companies_text'], 'alarmas')
        self.assertEqual(int(query_params['category']), category.id)
        self.assertEqual(int(query_params['product']), product.id)
        self.assertEqual(int(query_params['service']), service.id)
