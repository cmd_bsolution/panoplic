from django.contrib.auth import get_user_model

import factory

from companies.tests.factories import CountryFactory, ProvinceFactory, CityFactory, CompanyFactory
from ..models import UserProfile

UserModel = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserModel
        django_get_or_create = ('username',)

    username = factory.Sequence("username{}".format)
    email = factory.Sequence('email{}@example.com'.format)
    first_name = 'First Name'
    last_name = 'Last Name'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        """Override the default ``_create`` with our custom call."""
        manager = cls._get_manager(model_class)
        # The default would use ``manager.create(*args, **kwargs)``
        return manager.create_user(*args, **kwargs)


class UserProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = UserProfile

    user = factory.SubFactory(UserFactory)
    country = factory.SubFactory(CountryFactory)
    province = factory.LazyAttribute(lambda o: ProvinceFactory.create(country=o.country))
    city = factory.LazyAttribute(lambda o: CityFactory.create(province=o.province))
    birthday = '2010-1-1'
    gender = 'M'
    phone = '123456789'
    avatar = factory.django.ImageField(color='blue')
