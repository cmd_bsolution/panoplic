from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend


class EmailBackend(ModelBackend):

    def authenticate(self, username=None, password=None):
        user = get_user_model()
        try:
            user = user.objects.get(email=username)
            if user.check_password(password):
                return user
        except user.DoesNotExist:
            return None
