from django.conf import settings
from django.core.mail import send_mail
from django.contrib.sites.models import Site
from django.dispatch.dispatcher import receiver
from django.template import Context
from django.template.loader import get_template

from registration.signals import user_activated


@receiver(user_activated, dispatch_uid='mail sender')
def send_confirm_activation_email(sender, **kwargs):
    user = kwargs.get('user')
    site = Site.objects.get_current()
    context_dict = {
        'site': site,
        'user': user,
        'STATIC_URL': settings.STATIC_URL,
    }
    message = get_template('email_templates/confirm_activation.txt')\
        .render(Context(context_dict))
    html_message = get_template('email_templates/confirm_activation.html')\
        .render(Context(context_dict))
    subject = get_template('email_templates/confirm_activation-subject.txt')\
        .render(Context(context_dict))
    send_mail(
        subject=subject,
        message=message,
        html_message=html_message,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        fail_silently=True,
    )
