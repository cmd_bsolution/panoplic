from django import forms
from django.conf import settings
from django.template.defaultfilters import filesizeformat
from django.utils.translation import ugettext_lazy as _


def validate_size(uploaded_file):
    """
    Checks file type and size against available CONTENT_TYPES and
    MAX_UPLOAD_SIZE respectively.
    """
    content_type = uploaded_file.content_type.split('/')[0]
    if content_type in settings.CONTENT_TYPES:
        if uploaded_file._size > settings.MAX_UPLOAD_SIZE:
            raise forms.ValidationError(
                _('Please keep filesize under %(max)s. '
                  'Current filesize is %(current)s' %
                  {'max': filesizeformat(settings.MAX_UPLOAD_SIZE),
                   'current': filesizeformat(uploaded_file._size)})
            )
    else:
        raise forms.ValidationError(_('File type is not supported'))


def raw_value(form, fieldname):
    """
    Returns the raw_value for a particular field name. This is just a
    convenient wrapper around widget.value_from_datadict.
    """
    field = form.fields[fieldname]
    prefix = form.add_prefix(fieldname)
    return field.widget.value_from_datadict(form.data, form.files, prefix)


def val_for(form, field_name):
    if form.instance:
        field_id = form.instance.__getattribute__(field_name + '_id')
    else:
        field_id = None
    return raw_value(form, field_name) or field_id
