from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied


class LoginRequiredMixin(object):
    """Mixin that applies the login_required decorator to a CBV."""

    @classmethod
    def as_view(cls, **kwargs):
        view = super().as_view(**kwargs)
        return login_required(view)


class TestOwnerMixin(LoginRequiredMixin):
    """
    Mixin for CBVs that inherit from SingleObjectMixin. Checks whether the
    current user is the object's owner, otherwise returning an HTTP 403
    response.
    """

    def dispatch(self, request, *args, **kwargs):
        if self.get_object().user != request.user:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)
