from .forms import SearchForm


def search(request):
    """Get a SearchForm instance in the RequestContext."""
    search_form = SearchForm()
    return {'search_form': search_form}
