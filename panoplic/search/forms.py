from django import forms
from django.utils.translation import ugettext_lazy as _

from companies.models import Category, Product, Service
from .models import Search


class SearchForm(forms.ModelForm):

    category = forms.ModelChoiceField(
        label=_(u'Category'), empty_label=_(u'Category'),
        queryset=Category.objects.all(),
        required=False,
    )
    product = forms.ModelChoiceField(
        label=_('Producto'), empty_label=_('Producto/Servicio'),
        queryset=Product.objects.order_by('name'),
        required=False,
    )
    service = forms.ModelChoiceField(
        label=_(u'Service'), empty_label=_(u'Service Type'),
        queryset=Service.objects.all(),
        required=False,
    )

    class Meta:
        model = Search
        fields = ('name', 'category', 'product', 'service', 'companies_text',
                  'places_text')
