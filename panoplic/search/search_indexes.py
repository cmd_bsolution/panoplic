from haystack import indexes

from companies.models import Company


class CompanyIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    country = indexes.CharField(use_template=True)
    location = indexes.CharField(use_template=True)
    category = indexes.CharField(use_template=True)
    product = indexes.CharField(use_template=True)
    service = indexes.CharField(use_template=True)
    mark = indexes.FloatField(model_attr='mark')

    def get_model(self):
        return Company

    def should_update(self, instance, **kwargs):
        return instance.active
