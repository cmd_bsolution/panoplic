from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from companies.models import Category, Product, Service


class Search(models.Model):

    name = models.CharField(max_length=50, blank=True)
    companies_text = models.CharField(max_length=50, blank=True)
    places_text = models.CharField(max_length=50, blank=True)
    category = models.ForeignKey(Category, blank=True, null=True)
    product = models.ForeignKey(Product, blank=True, null=True)
    service = models.ForeignKey(Service, blank=True, null=True)

    class Meta:
        verbose_name = _('Search')
        verbose_name_plural = _('Searches')

    def get_absolute_url(self):
        q = ('?companies_text=%s&places_text=%s&category=%s&product=%s'
             '&service=%s') % \
            (self.companies_text, self.places_text, self.category_id or '',
             self.product_id or '', self.service_id or '')
        return reverse('search') + q
