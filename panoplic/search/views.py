import json
from itertools import chain

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from django.shortcuts import render
from django.utils.translation import ugettext as _

from haystack.query import SearchQuerySet

from companies.models import Company, Province, City, Service, Product
from .forms import SearchForm


def model_generator(sqs):
    """Yield each model instance in a given SearchQuerySet."""
    for i in sqs:
        yield i.object


def search(request):
    """Serve the search results page, listing found companies."""
    form = SearchForm(request.GET)
    if form.is_valid():
        c = form.cleaned_data['companies_text']
        p = form.cleaned_data['places_text']

        query = SearchQuerySet().filter(country=request.subdomain)\
                                .order_by('-mark', 'text')

        if len(c) > 0:
            query = query.filter(content=c)
        if len(p) > 0:
            query = query.filter(location=p)
        if form.cleaned_data['category']:
            query = query.filter(category=form.cleaned_data['category'])
        if form.cleaned_data['product']:
            query = query.filter(product=form.cleaned_data['product'])
        if form.cleaned_data['service']:
            query = query.filter(service=form.cleaned_data['service'])
    else:
        raise Exception("Invalid search")

    paginator = Paginator(query.all(), 10)
    page = request.GET.get('page', 1)
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        page_obj = paginator.page(1)
    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)

    # Create a companies generator (only if there is at least one result)
    companies = model_generator(page_obj) if paginator.count else None

    return render(request, 'companies.html', {
        'companies': companies,
        'page_obj': page_obj,
        'form': form
    })


def _make_autocomplete_dict(name, query, model_name):
    """Returns a dictionary with the appropiate k: v pairs for autocomplete."""
    query_index = name.lower().rfind(query.lower())
    label = name[:query_index] + "<b style='font-weight:bold;'>" +\
        name[query_index:query_index+len(query)] + '</b>' +\
        name[query_index+len(query):]
    return {'label': label, 'value': name, 'category': model_name}


def search_companies(request):
    MAX_RESULTS = 3

    q = request.GET.get('term', "")
    local_companies = Company.objects.filter(country__code=request.subdomain)
    companies = list(local_companies.filter(name__istartswith=q).order_by('name'))
    companies.extend(list(local_companies.filter(name__icontains=q).exclude(name__istartswith=q).order_by('name')))
    services = Service.objects.filter(name__icontains=q).order_by('name').all()
    products = Product.objects.filter(name__icontains=q).order_by('name').all()

    results = [_make_autocomplete_dict(c.name, q, _('Company'))
               for c in companies[:MAX_RESULTS]] +\
              [_make_autocomplete_dict(s.name, q, _('Service'))
               for s in services[:MAX_RESULTS]] +\
              [_make_autocomplete_dict(p.name, q, _('Product'))
               for p in products[:MAX_RESULTS]]

    response_data = json.dumps(results)
    return HttpResponse(response_data)


def search_places(request):
    q = request.GET.get('term', "")
    country_code = request.subdomain
    cities = City.objects\
                 .filter(name__istartswith=q,
                         province__country__code=country_code)\
                 .distinct('name').values('name')
    provinces = Province.objects\
                        .filter(name__istartswith=q,
                                country__code=country_code)\
                        .distinct('name').values('name')
    results = sorted(chain(cities, provinces), key=lambda x: x.items())
    cities = City.objects\
                 .exclude(name__istartswith=q)\
                 .filter(name__icontains=q,
                         province__country__code=country_code)\
                 .distinct('name').values('name')
    provinces = Province.objects\
                        .exclude(name__istartswith=q)\
                        .filter(name__icontains=q, country__code=country_code)\
                        .distinct('name').values('name')
    results.extend(sorted(chain(cities, provinces), key=lambda x: x.items()))
    results = [r['name'] for r in results[:10]]  # return 10 results max
    response_data = json.dumps(results)
    return HttpResponse(response_data)
