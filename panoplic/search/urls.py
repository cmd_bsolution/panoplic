from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.search, name='search'),
    url(r'^companies/$', views.search_companies),
    url(r'^places/$', views.search_places),
]
