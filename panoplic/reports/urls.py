from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^report-company/(?P<pk>\d+)/$',
        views.ReportCompany.as_view(),
        name='report_company'
    ),
    url(
        r'^report-review/(?P<pk>\d+)/$',
        views.ReportReview.as_view(),
        name='report_review'
    ),
]
