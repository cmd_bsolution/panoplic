from django.http import JsonResponse
from django.utils.translation import ugettext as _
from django.views.generic import CreateView

from companies.models import Company
from reviews.models import Review
from utils.views import LoginRequiredMixin
from .forms import CompanyReportForm, ReviewReportForm


class ReportCompany(LoginRequiredMixin, CreateView):
    """Create a CompanyReport object."""

    form_class = CompanyReportForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.target = Company.objects.get(id=self.kwargs['pk'])
        self.object.save()
        self.object.send_report_email()

        message = _("The staff has been notified. Thank you for your feedback")

        return JsonResponse({'message': message})


class ReportReview(LoginRequiredMixin, CreateView):
    """Create a ReviewReport object."""

    form_class = ReviewReportForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.target = Review.objects.get(id=self.kwargs['pk'])
        self.object.save()
        self.object.send_report_email()

        message = _("The staff has been notified. Thank you for your feedback")

        return JsonResponse({'message': message})
