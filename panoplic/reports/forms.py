from django import forms

from .models import CompanyReport, ReviewReport


class CompanyReportForm(forms.ModelForm):
    motive = forms.ChoiceField(choices=CompanyReport.MOTIVE_CHOICES)

    class Meta:
        model = CompanyReport
        fields = ('motive', 'text')


class ReviewReportForm(forms.ModelForm):
    motive = forms.ChoiceField(choices=ReviewReport.MOTIVE_CHOICES)

    class Meta:
        model = ReviewReport
        fields = ('motive', 'text')
