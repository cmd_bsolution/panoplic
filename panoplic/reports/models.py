from django.conf import settings
from django.core.mail import mail_managers
from django.db import models
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _

from companies.models import Company
from reviews.models import Review


class ContentReport(models.Model):
    """
    Generic class for inappropriate content reports, intended to be
    subclassed by reports on specific objects.
    """
    motive = models.CharField(max_length=25)
    text = models.TextField(blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    submitted_on = models.DateTimeField(auto_now_add=True)
    target = None

    def send_report_email(self):
        """
        Notify managers that a report has been created.
        """
        object_name = str(self.target._meta.verbose_name).lower()
        context_dict = {
            'user': self.user,
            'target': self.target,
            'text': self.text,
            'motive': dict(self.MOTIVE_CHOICES)[self.motive]
        }
        message = render_to_string(
            'email_templates/%s_report.txt' % object_name,
            context_dict
        )
        html_message = render_to_string(
            'email_templates/%s_report.html' % object_name,
            context_dict
        )
        mail_managers(
            subject="New %s content report" % object_name,
            message=message,
            html_message=html_message,
            fail_silently=True
        )

    def __str__(self):
        return "Report on {} by {}".format(self.target, self.user)


class CompanyReport(ContentReport):

    MOTIVE_CHOICES = (
        ("BREAKS_TERMS", _("Breaks the site's Terms")),
        ("DOES_NOT_EXIST", _("Does not exist")),
        ("WRONG_INFO", _("Wrong informationg")),
    )

    target = models.ForeignKey(Company)


class ReviewReport(ContentReport):

    MOTIVE_CHOICES = (
        ("BREAKS_TERMS", _("Breaks the site's Terms")),
        ("BY_OWNER", _("Submitted by owner or staff")),
        ("BY_COMPETITOR", _("Submitted by a competitor")),
    )

    target = models.ForeignKey(Review)
