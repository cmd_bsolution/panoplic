up:
	docker-compose -f dev.yml up -d $(filter-out $@,$(MAKECMDGOALS))

build:
	docker-compose -f dev.yml build $(filter-out $@,$(MAKECMDGOALS))

run:
	docker-compose -f dev.yml run --rm $(filter-out $@,$(MAKECMDGOALS))

restart:
	docker-compose -f dev.yml restart $(filter-out $@,$(MAKECMDGOALS))

shell:
	docker-compose -f dev.yml run --rm django python manage.py shell_plus

bash:
	docker-compose -f dev.yml run --rm django bash

makemigrations:
	docker-compose -f dev.yml run --rm django python manage.py makemigrations $(filter-out $@,$(MAKECMDGOALS))

migrate:
	docker-compose -f dev.yml run --rm django python manage.py migrate $(filter-out $@,$(MAKECMDGOALS))

urls:
	docker-compose -f dev.yml run --rm django python manage.py show_urls

# --- Haystack ---
update_index:
	docker-compose -f dev.yml run --rm django python manage.py update_index

rebuild_index:
	docker-compose -f dev.yml run --rm django python manage.py rebuild_index

logs:
	docker-compose -f dev.yml logs -f --tail=70 $(filter-out $@,$(MAKECMDGOALS))

test:
	docker-compose -f dev.yml run --rm django python manage.py test $(filter-out $@,$(MAKECMDGOALS))

coverage:
	docker-compose -f dev.yml run --rm django coverage run ./manage.py test

coverage_report:
	docker-compose -f dev.yml run --rm django coverage html

debug:
	docker-compose -f dev.yml run --service-port --rm $(filter-out $@,$(MAKECMDGOALS))
