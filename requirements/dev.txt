-r base.txt

django-debug-toolbar==1.8
ipython
ipdb

# Test packages
selenium
coverage
django_coverage_plugin
factory_boy
